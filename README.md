# math

Набор геометрических и математических объектов и операций над ними

---

### Углы (angle)

```
val d = degreeOf(180f) //создать угол в градусах
val r = radianOf(3.14f) //создать угол в радианах
```
---

### Цвета (color)
```
val v1 = Color.WHITE
val v2 = RgbColor3f(1f, 1f, 1f)
val v3 = RgbColor3b(255u, 255u, 255u)
val v4 = RgbaColor4f(1f, 1f, 1f, 1f)
val v5 = HsvColor3f(0f, 0f, 1f)
```
---
### Массивы (array)
Одномерные, двумерные, трехмерные массивы.
Трехмерный байтовый массив можно использовать например для описания RGB картинки - три размера это ширина, высота, и глубина цвета картинки.

---

### Коллекции (collection)
- [LRUCache](src/commonMain/kotlin/ru/casperix/math/collection/LRUCache.kt)
- [DoublyLinkedList](src/commonMain/kotlin/ru/casperix/math/collection/DoublyLinkedList.kt)

---

### Axis-algned

 [Box2f](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%axis_aligned%2Ffloat32%2FBox2f.kt),
 [Box3f](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%axis_aligned%2Ffloat32%2FBox3f.kt),
 [Box2i](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%axis_aligned%2Fint32%2FBox2i.kt), 
 [Box3i](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%axis_aligned%2Fint32%2FBox3i.kt)
 -- `AABBox` ограничивающие боксы

---

### Интерполяция (interpolation)
Интерполяция углов, цветов, векторов, чисел.
Есть готовые функции интерполяции (линейные, косинус, хермит, и многие другие). 
Можно использовать свою функцию интерполяции.

```
InterpolationFloat.color(Color.RED, Color.GREEN, 0.5f, cosineInterpolatef)
```

---

### Пересечения (intersection)

Есть набор функций для поиска области пересечения, например:
```
val intersection = Intersection2Float.getSegmentWithSegment(
    Line2f(Vector2f(0f, 0f), Vector2f(2f, 2f)),
    Line2f(Vector2f(2f, 0f), Vector2f(0f, 2f)),
)
//  intersection == Vector2f(1f, 1f)
```

Также есть функции определения наличия пересечения, например:
```
val hasIntersection = Intersection2Float.hasPointWithQuad(
    Vector2f.ONE,
    Quad2f(
        Vector2f(0f, 0f),
        Vector2f(2f, 0f),
        Vector2f(2f, 2f),
        Vector2f(0f, 2f),
    )
)
//  hasIntersection == true
```

---

### Трансформация (transform)

[transformOf](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%2Ftransform%2FTransform.kt) - создать трансформацию

Трансформация - описание положения объекта в 3Д пространстве (в отличие от матрицы хранит положение, вращение, масштабирование явно)

---

### random

Генерация случайных углов, цветов, полярных координат, векторов, фигур.

---

### vector


[vectorOf](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fmath%2Fvector%2FVector.kt) - создать вектор из двух или трех чисел

* поддерживаемые типы чисел `double`, `float`, `int`


- `Vector2d` - два числа - 2D пространство
- `Vector3*` - три числа - 3D пространство


- `Vector2d`, `Vector3d` - тип чисел: `double`
- `Vector2f`, `Vector3f` - тип чисел: `float`
- `Vector2i`, `Vector3i` - тип чисел: `int`

Вектор - упорядоченный набор чисел и оперций над ними

---

### Квадратные матрицы

* поддерживаемые размерности 3х3, 4х4
* поддерживаемые типы чисел `double`, `float`

[Matrix3f](src/commonMain/kotlin/ru/casperix/math/quad_matrix/float32/Matrix3f.kt),
[Matrix4f](src/commonMain/kotlin/ru/casperix/math/quad_matrix/float32/Matrix4f.kt),
[Matrix3d](src/commonMain/kotlin/ru/casperix/math/quad_matrix/float32/Matrix3d.kt),
[Matrix4d](src/commonMain/kotlin/ru/casperix/math/quad_matrix/float32/Matrix4d.kt)

```
val identity = Matrix3f.IDENTITY
val some = Matrix3f.scale(1f, 2f) * Matrix3f.rotate(DegreeFloat(90f)) * Matrix3f.translate(3f, 4f)
val alternative = Matrix3f.compose(Vector2f(1f, 2f), DegreeFloat(90f), Vector2f(3f, 4f))
val hardcode = matrixOf(1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f)
```
Для других типов и размерностей пример будет аналогичен.

---


## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`sonatypeStagingProfileId`,
`sonatypeUsername`,
`sonatypePassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.


Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin