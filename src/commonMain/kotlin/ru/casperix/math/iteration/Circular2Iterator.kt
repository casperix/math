package ru.casperix.math.iteration


import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i

class Circular2Iterator(val center: Vector2i, val minRange: Int, val maxRange: Int, val step:Int = 1) : Iterator<Vector2i> {
	var position: Vector2i? = null
	var lastRadius = minRange - step
	var iterator: Box2FaceIterator? = null

	init {
		if (step < 1) throw Error("Step must be greater or equal than one (now: $step)")
		if (minRange < 0) throw Error("Min radius must be non negative (now: $minRange)")
		if (maxRange < 0) throw Error("Max radius must be non negative (now: $maxRange)")
		if (minRange > maxRange) throw Error("Min radius ($minRange) must be non greater than max radius ($maxRange)")

		nextIterator()
		position = getNext()
	}

	private fun nextIterator() {
		lastRadius += step
		if (lastRadius <= maxRange) {
			iterator = Box2FaceIterator(Box2i.byRadius(center, Vector2i(lastRadius)), step)
		} else {
			iterator = null
		}
	}

	private fun getNext(): Vector2i? {
		while (true) {
			val iterator = iterator ?: return null
			if (iterator.hasNext()) {
				return iterator.next()
			} else {
				nextIterator()
			}
		}
	}

	override fun next(): Vector2i {
		val last = position ?: throw Error("Invalid next. You must check hasNext first")
		position = getNext()
		return last
	}

	override fun hasNext(): Boolean {
		return position != null
	}

}