package casperix.math.function

fun <T> emptyIteratorOf(): Iterator<T> {
	return object : Iterator<T> {
		override fun hasNext(): Boolean {
			return false
		}

		override fun next(): T {
			throw Error("Its empty iterator. First you need check hasNext")
		}
	}
}