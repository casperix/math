package ru.casperix.math.iteration


import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i

/**
 * 	Итератор по поверхности бокса
 */
class Box2FaceIterator(val source: Box2i, val step: Int = 1) : Iterator<Vector2i> {
	private var x: Int
	private var y: Int
	private var next: Vector2i?
	private var xStep = step
	private var bigXStep = source.max.x - source.min.x

	init {
		x = source.min.x
		y = source.min.y
		next = Vector2i(x, y)
	}

	/**
	 * Returns the next element in the iteration.
	 */
	override operator fun next(): Vector2i {
		val result = next!!

		while (true) {
			x += xStep
			if (x > source.max.x) {
				x = source.min.x
				y += step
				updateXStep()
				if (y > source.max.y) {
					next = null
					return result
				}
			}
			val pos = Vector2i(x, y)
			if (source.isSide(pos)) {
				next = pos
				return result
			}
		}
	}

	private fun updateXStep() {
		if ((y == source.min.y || y == source.max.y)) {
			xStep = step
		} else {
			xStep = bigXStep
		}
	}


	/**
	 * Returns `true` if the iteration has more elements.
	 */
	override operator fun hasNext(): Boolean {
		return next != null
	}
}