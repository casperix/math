package ru.casperix.math.vector.float32

import kotlinx.serialization.Serializable
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.vector.api.AbstractVector2
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.format.FormatType
import ru.casperix.misc.toPrecision
import kotlin.math.*

@Serializable
data class Vector2f(override val x: Float, override val y: Float) : AbstractVector2<Vector2f, Float> {
    constructor() : this(0f)

    constructor(i: Float) : this(i, i)

    companion object {
        val NaN = Vector2f(Float.NaN)
        val ZERO = Vector2f(0.0f)
        val HALF = Vector2f(0.5f)
        val ONE = Vector2f(1.0f)
        val XY = ONE

        val X = Vector2f(1f, 0f)
        val Y = Vector2f(0f, 1f)
    }

    override val xAxis: Vector2f get() = Vector2f(x, 0f)

    override val yAxis: Vector2f get() = Vector2f(0f, y)

    fun rotate(angle: DegreeFloat): Vector2f {
        return rotate(angle.toRadian())
    }

    fun rotate(angle: RadianFloat): Vector2f {
        val polar = toPolar()
        return polar.copy(angle = polar.angle + angle).toDecart()
    }

    override fun axisProjection(useXAxis: Boolean): Vector2f {
        return if (useXAxis) xAxis else yAxis
    }

    override fun volume(): Float {
        return x * y
    }

    override fun distTo(other: Vector2f): Float {
        //  Manual Inline Hi Again
        return hypot(x - other.x, y - other.y)
    }

    override fun lengthOne(): Float {
        return abs(x) + abs(y)
    }

    override fun length(): Float {
        return hypot(x, y)
    }

    override fun lengthInf(): Float {
        return max(abs(x), abs(y))
    }

    override fun lengthSquared(): Float {
        return x * x + y * y
    }

    override fun absoluteMinimum(): Float {
        return minOf(abs(x), abs(y))
    }

    override fun absoluteMaximum(): Float {
        return maxOf(abs(x), abs(y))
    }

    override val sign: Vector2f get() = Vector2f(x.sign, y.sign)

    override val absoluteValue: Vector2f get() = Vector2f(x.absoluteValue, y.absoluteValue)

    override fun dot(value: Vector2f): Float {
        return (this.x * value.x + this.y * value.y)
    }

    override fun mod(other: Vector2f): Vector2f {
        return Vector2f(x.mod(other.x), y.mod(other.y))
    }

    override fun upper(other: Vector2f): Vector2f {
        return Vector2f(max(x, other.x), max(y, other.y))
    }

    override fun lower(other: Vector2f): Vector2f {
        return Vector2f(min(x, other.x), min(y, other.y))
    }

    fun clamp(min: Vector2f, max: Vector2f): Vector2f {
        return upper(min).lower(max)
    }

    fun clamp(limit: Box2f): Vector2f {
        return clamp(limit.min, limit.max)
    }

    override operator fun plus(position: Vector2f): Vector2f {
        return Vector2f(x + position.x, y + position.y)
    }

    override operator fun minus(position: Vector2f): Vector2f {
        return Vector2f(x - position.x, y - position.y)
    }

    override operator fun div(value: Float): Vector2f {
        return Vector2f(x / value, y / value)
    }

    override operator fun div(value: Vector2f): Vector2f {
        return Vector2f(x / value.x, y / value.y)
    }

    override operator fun times(value: Float): Vector2f {
        return Vector2f(x * value, y * value)
    }

    override operator fun times(value: Vector2f): Vector2f {
        return Vector2f(x * value.x, y * value.y)
    }

    override operator fun unaryMinus(): Vector2f {
        return Vector2f(-x, -y)
    }

    override operator fun rem(value: Vector2f): Vector2f {
        return Vector2f(x % value.x, y % value.y)
    }

    override operator fun rem(value: Float): Vector2f {
        return Vector2f(x % value, y % value)
    }

    override fun greater(other: Vector2f): Boolean {
        return x > other.x && y > other.y
    }

    override fun greaterOrEq(other: Vector2f): Boolean {
        return x >= other.x && y >= other.y
    }

    override fun less(other: Vector2f): Boolean {
        return x < other.x && y < other.y
    }

    override fun lessOrEq(other: Vector2f): Boolean {
        return x <= other.x && y <= other.y
    }

    fun addDimension(z: Float): Vector3f {
        return Vector3f(x, y, z)
    }

    override fun normalize(): Vector2f {
        val len = length().toFloat()
        return Vector2f(x / len, y / len)
    }

    override fun toVector2d(): Vector2d {
        return Vector2d(x.toDouble(), y.toDouble())
    }

    override fun toVector2i(): Vector2i {
        return Vector2i(x.toInt(), y.toInt())
    }

    override fun toVector2f(): Vector2f {
        return this
    }

    fun round(): Vector2f {
        return Vector2f(x.roundToInt().toFloat(), y.roundToInt().toFloat())
    }

    fun roundToVector2i(): Vector2i {
        return Vector2i(x.roundToInt(), y.roundToInt())
    }

    fun ceilToVector2i(): Vector2i {
        return Vector2i(x.ceilToInt(), y.ceilToInt())
    }

    fun cross(other: Vector2f): Float {
        return x * other.y - other.x * y
    }

    fun toPolar(): PolarCoordinateFloat {
        val range = length()
        val angle = RadianFloat.byDirection(this)
        return PolarCoordinateFloat(range, angle)
    }


    fun isFinite(): Boolean {
        return x.isFinite() && y.isFinite()
    }

    fun expand(z: Float): Vector3f {
        return Vector3f(x, y, z)
    }

    override fun half(): Vector2f {
        return this * 0.5f
    }

    @Deprecated(message = "Use format instead")
    fun toPrecision(precision: Int): String {
        return "(${x.toPrecision(precision)}; ${y.toPrecision(precision)})"
    }

    override fun toString(): String {
        return format()
    }

    fun format(type: FormatType = FormatType.NORMAL, precision: Int = 2): String {
        val sx = x.toPrecision(precision)
        val sy = y.toPrecision(precision)
        return when (type) {
            FormatType.DETAIL -> "Vector2f(x=$sx, y=$sy)"
            FormatType.NORMAL -> "V2f($sx, $sy)"
            FormatType.SHORT -> "${sx}x$sy"
        }
    }

    fun component(index: Int): Float {
        return when (index) {
            0 -> x
            1 -> y
            else -> throw Error("Only 2 components enabled")
        }
    }
}
