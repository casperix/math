package ru.casperix.math.vector.float64

import ru.casperix.math.axis_aligned.float64.Box3d
import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.vector.api.AbstractVector3
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.math.vector.uint32.Vector3u
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector3d(override val x: Double, override val y: Double, override val z: Double) :
    AbstractVector3<Vector3d, Double> {
    constructor() : this(0.0)

    constructor(i: Double) : this(i, i, i)

    companion object {
        val NaN = Vector3d(Double.NaN)
        val ZERO = Vector3d(0.0)
        val HALF = Vector3d(0.5)
        val ONE = Vector3d(1.0)
        val XYZ = ONE

        val X = Vector3d(1.0, 0.0, 0.0)
        val Y = Vector3d(0.0, 1.0, 0.0)
        val Z = Vector3d(0.0, 0.0, 1.0)
        val YZ = Vector3d(0.0, 1.0, 1.0)
        val XZ = Vector3d(1.0, 0.0, 1.0)
        val XY = Vector3d(1.0, 1.0, 0.0)
    }

    override val xAxis: Vector3d get() = Vector3d(x, 0.0, 0.0)
    override val yAxis: Vector3d get() = Vector3d(0.0, y, 0.0)
    override val zAxis: Vector3d get() = Vector3d(0.0, 0.0, z)

    override fun volume(): Double {
        return x * y * z
    }

    override fun distTo(other: Vector3d): Double {
        return (this - other).length()
    }

    override fun lengthOne(): Double {
        return abs(x) + abs(y) + abs(z)
    }

    override fun length(): Double {
        return sqrt(x * x + y * y + z * z)
    }

    override fun lengthInf(): Double {
        return maxOf(abs(x), abs(y), abs(z))
    }

    override fun lengthSquared(): Double {
        return x * x + y * y + z * z
    }

    override fun absoluteMinimum(): Double {
        return minOf(abs(x), abs(y), abs(z))
    }

    override fun absoluteMaximum(): Double {
        return maxOf(abs(x), abs(y), abs(z))
    }

    override val sign: Vector3d get() = Vector3d(x.sign, y.sign, z.sign)

    override val absoluteValue: Vector3d get() = Vector3d(x.absoluteValue, y.absoluteValue, z.absoluteValue)

    override fun dot(value: Vector3d): Double {
        return (this.x * value.x + this.y * value.y + this.z * value.z)
    }

    override fun mod(other: Vector3d): Vector3d {
        return Vector3d(x.mod(other.x), y.mod(other.y), z.mod(other.z))
    }

    override fun upper(other: Vector3d): Vector3d {
        return Vector3d(max(x, other.x), max(y, other.y), max(z, other.z))
    }

    override fun lower(other: Vector3d): Vector3d {
        return Vector3d(min(x, other.x), min(y, other.y), min(z, other.z))
    }

    fun clamp(min: Vector3d, max: Vector3d): Vector3d {
        return upper(min).lower(max)
    }

    fun clamp(area: Box3d): Vector3d {
        return clamp(area.min, area.max)
    }

    override operator fun plus(position: Vector3d): Vector3d {
        return Vector3d(x + position.x, y + position.y, z + position.z)
    }

    override operator fun minus(position: Vector3d): Vector3d {
        return Vector3d(x - position.x, y - position.y, z - position.z)
    }

    override operator fun div(value: Double): Vector3d {
        return Vector3d(x / value, y / value, z / value)
    }

    override operator fun div(value: Vector3d): Vector3d {
        return Vector3d(x / value.x, y / value.y, z / value.z)
    }

    override operator fun times(value: Double): Vector3d {
        return Vector3d(x * value, y * value, z * value)
    }

    override operator fun times(value: Vector3d): Vector3d {
        return Vector3d(x * value.x, y * value.y, z * value.z)
    }

    override operator fun unaryMinus(): Vector3d {
        return this * (-1.0)
    }

    override operator fun rem(value: Vector3d): Vector3d {
        return Vector3d(x % value.x, y % value.y, z % value.z)
    }

    override operator fun rem(value: Double): Vector3d {
        return Vector3d(x % value, y % value, z % value)
    }

    override fun greater(other: Vector3d): Boolean {
        return x > other.x && y > other.y && z > other.z
    }

    override fun greaterOrEq(other: Vector3d): Boolean {
        return x >= other.x && y >= other.y && z >= other.z
    }

    override fun less(other: Vector3d): Boolean {
        return x < other.x && y < other.y && z < other.z
    }

    override fun lessOrEq(other: Vector3d): Boolean {
        return x <= other.x && y <= other.y && z <= other.z
    }

//	fun addDimension(w: Double): Vector4d {
//		return Vector4d(x, y, z, w)
//	}

    override fun normalize(): Vector3d {
        val len = length()
        return Vector3d(x / len, y / len, z / len)
    }

    fun getQuaternion(): QuaternionDouble {
        return QuaternionDouble(x, y, z, 0.0)
    }

    override fun toVector3f(): Vector3f {
        return Vector3f(x.toFloat(), y.toFloat(), z.toFloat())
    }

    override fun toVector3d(): Vector3d {
        return this
    }

    override fun toVector3i(): Vector3i {
        return Vector3i(x.toInt(), y.toInt(), z.toInt())
    }

    override fun toVector3u(): Vector3u {
        return Vector3u(x.toUInt(), y.toUInt(), z.toUInt())
    }

    fun round(): Vector3d {
        return Vector3d(x.roundToInt().toDouble(), y.roundToInt().toDouble(), z.roundToInt().toDouble())
    }

    fun roundToVector3i(): Vector3i {
        return Vector3i(x.roundToInt(), y.roundToInt(), z.roundToInt())
    }

    fun ceilToVector3i(): Vector3i {
        return Vector3i(x.ceilToInt(), y.ceilToInt(), z.ceilToInt())
    }

    fun cross(value: Vector3d): Vector3d {
        return Vector3d(this.y * value.z - this.z * value.y, this.z * value.x - this.x * value.z, this.x * value.y - this.y * value.x)
    }

    fun toPrecision(precision: Int): String {
        return "${x.toPrecision(precision)}; ${y.toPrecision(precision)}; ${z.toPrecision(precision)}"
    }

    fun isFinite(): Boolean {
        return x.isFinite() && y.isFinite() && z.isFinite()
    }

    fun getXY(): Vector2d {
        return Vector2d(x, y)
    }

    fun getYZ(): Vector2d {
        return Vector2d(y, z)
    }

    fun getXZ(): Vector2d {
        return Vector2d(x, z)
    }

    fun expand(w: Double): Vector4d {
        return Vector4d(x, y, z, w)
    }

    override fun half(): Vector3d {
        return this * 0.5
    }

    override fun toString(): String {
        return "V3d(x=${x.toPrecision(2)}, y=${y.toPrecision(2)}, z=${z.toPrecision(2)})"
    }

    fun component(index: Int): Double {
        return when (index) {
            0 -> x
            1 -> y
            2 -> z
            else -> throw Error("Only 3 components enabled")
        }
    }
}


