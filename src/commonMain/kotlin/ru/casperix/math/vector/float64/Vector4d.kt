package ru.casperix.math.vector.float64

import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.vector.api.AbstractVector4
import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.int32.Vector4i
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable
import kotlin.math.*

@Serializable
data class Vector4d(override val x: Double, override val y: Double, override val z: Double, override val w: Double) :
    AbstractVector4<Vector4d, Double> {
    constructor() : this(0.0)

    constructor(i: Double) : this(i, i, i, i)

    companion object {
        val NaN = Vector4d(Double.NaN)
        val ZERO = Vector4d(0.0)
        val HALF = Vector4d(0.5)
        val ONE = Vector4d(1.0)
        val XYZW = ONE

        val X = Vector4d(1.0, 0.0, 0.0, 0.0)
        val Y = Vector4d(0.0, 1.0, 0.0, 0.0)
        val Z = Vector4d(0.0, 0.0, 1.0, 0.0)
        val W = Vector4d(0.0, 0.0, 0.0, 1.0)
    }

    override val xAxis: Vector4d get() = Vector4d(x, 0.0, 0.0, 0.0)
    override val yAxis: Vector4d get() = Vector4d(0.0, y, 0.0, 0.0)
    override val zAxis: Vector4d get() = Vector4d(0.0, 0.0, z, 0.0)
    override val wAxis: Vector4d get() = Vector4d(0.0, 0.0, 0.0, w)

    override fun volume(): Double {
        return x * y * z * w
    }

    override fun distTo(other: Vector4d): Double {
        return (this - other).length()
    }

    override fun lengthOne(): Double {
        return abs(x) + abs(y) + abs(z) + abs(w)
    }

    override fun length(): Double {
        return sqrt(x * x + y * y + z * z + w * w)
    }

    override fun lengthInf(): Double {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun lengthSquared(): Double {
        return x * x + y * y + z * z + w * w
    }

    override fun absoluteMinimum(): Double {
        return minOf(abs(x), abs(y), abs(z), abs(w))
    }

    override fun absoluteMaximum(): Double {
        return maxOf(abs(x), abs(y), abs(z), abs(w))
    }

    override val sign: Vector4d get() = Vector4d(x.sign, y.sign, z.sign, w.sign)

    override val absoluteValue: Vector4d get() = Vector4d(x.absoluteValue, y.absoluteValue, z.absoluteValue, w.absoluteValue)

    override fun dot(value: Vector4d): Double {
        return (this.x * value.x + this.y * value.y + this.z * value.z + this.w * value.w)
    }

    override fun mod(other: Vector4d): Vector4d {
        return Vector4d(x.mod(other.x), y.mod(other.y), z.mod(other.z), w.mod(other.w))
    }

    override fun upper(other: Vector4d): Vector4d {
        return Vector4d(max(x, other.x), max(y, other.y), max(z, other.z), max(w, other.w))
    }

    override fun lower(other: Vector4d): Vector4d {
        return Vector4d(min(x, other.x), min(y, other.y), min(z, other.z), min(w, other.w))
    }

    fun clamp(min: Vector4d, max: Vector4d): Vector4d {
        return upper(min).lower(max)
    }

    override operator fun plus(position: Vector4d): Vector4d {
        return Vector4d(x + position.x, y + position.y, z + position.z, w + position.w)
    }

    override operator fun minus(position: Vector4d): Vector4d {
        return Vector4d(x - position.x, y - position.y, z - position.z, w - position.w)
    }

    override operator fun div(value: Double): Vector4d {
        return Vector4d(x / value, y / value, z / value, w / value)
    }

    override operator fun div(value: Vector4d): Vector4d {
        return Vector4d(x / value.x, y / value.y, z / value.z, w / value.w)
    }

    override operator fun times(value: Double): Vector4d {
        return Vector4d(x * value, y * value, z * value, w * value)
    }

    override operator fun times(value: Vector4d): Vector4d {
        return Vector4d(x * value.x, y * value.y, z * value.z, w * value.w)
    }

    override operator fun unaryMinus(): Vector4d {
        return this * (-1.0)
    }

    override operator fun rem(value: Vector4d): Vector4d {
        return Vector4d(x % value.x, y % value.y, z % value.z, w % value.w)
    }

    override operator fun rem(value: Double): Vector4d {
        return Vector4d(x % value, y % value, z % value, w % value)
    }

    override fun greater(other: Vector4d): Boolean {
        return x > other.x && y > other.y && z > other.z && w > other.w
    }

    override fun greaterOrEq(other: Vector4d): Boolean {
        return x >= other.x && y >= other.y && z >= other.z && w >= other.w
    }

    override fun less(other: Vector4d): Boolean {
        return x < other.x && y < other.y && z < other.z && w < other.w
    }

    override fun lessOrEq(other: Vector4d): Boolean {
        return x <= other.x && y <= other.y && z <= other.z && w <= other.w
    }

//	fun addDimension(w: Double): Vector4d {
//		return Vector4d(x, y, z, w)
//	}

    override fun normalize(): Vector4d {
        val len = length()
        return Vector4d(x / len, y / len, z / len, w / len)
    }

    fun getQuaternion(): QuaternionDouble {
        return QuaternionDouble(x, y, z, w)
    }

    override fun toVector4f(): Vector4f {
        return Vector4f(x.toFloat(), y.toFloat(), z.toFloat(), w.toFloat())
    }

    override fun toVector4i(): Vector4i {
        return Vector4i(x.toInt(), y.toInt(), z.toInt(), w.toInt())
    }

    override fun toVector4d(): Vector4d {
        return this
    }

    fun round(): Vector4d {
        return Vector4d(x.roundToInt().toDouble(), y.roundToInt().toDouble(), z.roundToInt().toDouble(), w.roundToInt().toDouble())
    }

    fun roundToVector4i(): Vector4i {
        return Vector4i(x.roundToInt(), y.roundToInt(), z.roundToInt(), w.roundToInt())
    }

    fun ceilToVector4i(): Vector4i {
        return Vector4i(x.ceilToInt(), y.ceilToInt(), z.ceilToInt(), w.ceilToInt())
    }

    fun toPrecision(precision: Int): String {
        return "${x.toPrecision(precision)}; ${y.toPrecision(precision)}; ${z.toPrecision(precision)}"
    }

    fun isFinite(): Boolean {
        return x.isFinite() && y.isFinite() && z.isFinite() && w.isFinite()
    }

    fun getXYZ(): Vector3d {
        return Vector3d(x, y, z)
    }

    fun getXYW(): Vector3d {
        return Vector3d(x, y, w)
    }

    fun getXZW(): Vector3d {
        return Vector3d(x, z, w)
    }

    fun getYZW(): Vector3d {
        return Vector3d(y, z, w)
    }

    override fun half(): Vector4d {
        return this * 0.5
    }

    override fun toString(): String {
        return "V3d(x=${x.toPrecision(2)}, y=${y.toPrecision(2)}, z=${z.toPrecision(2)}, w=${w.toPrecision(2)})"
    }

    fun component(index: Int): Double {
        return when (index) {
            0 -> x
            1 -> y
            2 -> z
            3 -> w
            else -> throw Error("Only 4 components enabled")
        }
    }
}


