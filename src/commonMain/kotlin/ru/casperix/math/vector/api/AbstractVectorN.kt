package ru.casperix.math.vector.api

import ru.casperix.math.vector.VectorArithmetic


typealias AbstractVector<Self> = AbstractVectorN<Self, *>

interface AbstractVectorN<Self : Any, Item:Number> : VectorArithmetic<Self, Item> {
    val sign: Self

    val absoluteValue: Self
    fun mod(other: Self): Self
    fun dot(value: Self): Item
    fun upper(other: Self): Self
    fun lower(other: Self): Self
    operator fun rem(value: Item): Self
    operator fun rem(value: Self): Self
    fun greater(other: Self): Boolean
    fun greaterOrEq(other: Self): Boolean
    fun less(other: Self): Boolean
    fun lessOrEq(other: Self): Boolean
    fun volume(): Item
    fun absoluteMinimum(): Item
    fun absoluteMaximum(): Item
    fun normalize(): Self

    fun half(): Self
}