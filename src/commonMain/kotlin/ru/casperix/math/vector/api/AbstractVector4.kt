package ru.casperix.math.vector.api

import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.float64.Vector4d
import ru.casperix.math.vector.int32.Vector4i

interface AbstractVector4<Self : Any, Item : Number> :
    AbstractVectorN<Self, Item> {
    val x: Item
    val y: Item
    val z: Item
    val w: Item

    val xAxis: Self
    val yAxis: Self
    val zAxis: Self
    val wAxis: Self

    fun toVector4d(): Vector4d
    fun toVector4f(): Vector4f
    fun toVector4i(): Vector4i
}