package ru.casperix.math.vector.api

import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.math.vector.uint32.Vector3u

interface AbstractVector3<Self : Any, Item:Number> : AbstractVectorN<Self, Item> {
    val x: Item
    val y: Item
    val z: Item

    val xAxis: Self
    val yAxis: Self
    val zAxis: Self

    fun toVector3d(): Vector3d
    fun toVector3f(): Vector3f
    fun toVector3i(): Vector3i

    fun toVector3u(): Vector3u
}