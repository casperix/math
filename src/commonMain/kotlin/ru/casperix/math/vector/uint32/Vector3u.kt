package ru.casperix.math.vector.uint32

import ru.casperix.math.vector.api.AbstractVector3
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector3i
import kotlinx.serialization.Serializable
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.math.sqrt

@Serializable
data class Vector3u(val x: UInt, val y: UInt, val z: UInt)  {
    constructor() : this(0u)

    constructor(i: UInt) : this(i, i, i)

    companion object {
        val ZERO = Vector3u(0u)
        val ONE = Vector3u(1u)
        val XYZ = ONE

        val X = Vector3u(1u, 0u, 0u)
        val Y = Vector3u(0u, 1u, 0u)
        val Z = Vector3u(0u, 0u, 1u)
        val YZ = Vector3u(0u, 1u, 1u)
        val XZ = Vector3u(1u, 0u, 1u)
        val XY = Vector3u(1u, 1u, 0u)
    }

    val xAxis: Vector3u get() = Vector3u(x, 0u, 0u)
    val yAxis: Vector3u get() = Vector3u(0u, y, 0u)
    val zAxis: Vector3u get() = Vector3u(0u, 0u, z)

    fun volume(): UInt {
        return x * y * z
    }

    fun distTo(other: Vector3u): UInt {
        return (this - other).length()
    }

    fun lengthOne(): UInt {
        return x + y + z
    }

    fun length(): UInt {
        return sqrt((x * x + y * y + z * z).toFloat()).roundToInt().toUInt()
    }

    fun lengthInf(): UInt {
        return maxOf(x, y, z)
    }

    fun lengthSquared(): UInt {
        return x * x + y * y + z * z
    }

    fun absoluteMinimum(): UInt {
        return minOf(x, y, z)
    }

    fun absoluteMaximum(): UInt {
        return maxOf(x, y, z)
    }

    val absoluteValue: Vector3u get() = this

    fun dot(value: Vector3u): UInt {
        return (this.x * value.x + this.y * value.y + this.z * value.z)
    }

    fun mod(other: Vector3u): Vector3u {
        return Vector3u(x.mod(other.x), y.mod(other.y), z.mod(other.z))
    }


    fun upper(other: Vector3u): Vector3u {
        return Vector3u(max(x, other.x), max(y, other.y), max(z, other.z))
    }

    fun lower(other: Vector3u): Vector3u {
        return Vector3u(min(x, other.x), min(y, other.y), min(z, other.z))
    }

    fun clamp(min: Vector3u, max: Vector3u): Vector3u {
        return upper(min).lower(max)
    }

    operator fun plus(position: Vector3u): Vector3u {
        return Vector3u(x + position.x, y + position.y, z + position.z)
    }

    operator fun minus(position: Vector3u): Vector3u {
        return Vector3u(x - position.x, y - position.y, z - position.z)
    }

    operator fun div(value: UInt): Vector3u {
        return Vector3u(x / value, y / value, z / value)
    }

    operator fun div(value: Vector3u): Vector3u {
        return Vector3u(x / value.x, y / value.y, z / value.z)
    }

    operator fun times(value: UInt): Vector3u {
        return Vector3u(x * value, y * value, z * value)
    }

    operator fun times(value: Vector3u): Vector3u {
        return Vector3u(x * value.x, y * value.y, z * value.z)
    }

    operator fun rem(value: Vector3u): Vector3u {
        return Vector3u(x % value.x, y % value.y, z % value.z)
    }

    operator fun rem(value: UInt): Vector3u {
        return Vector3u(x % value, y % value, z % value)
    }

    fun greater(other: Vector3u): Boolean {
        return x > other.x && y > other.y && z > other.z
    }

    fun greaterOrEq(other: Vector3u): Boolean {
        return x >= other.x && y >= other.y && z >= other.z
    }

    fun less(other: Vector3u): Boolean {
        return x < other.x && y < other.y && z < other.z
    }

    fun lessOrEq(other: Vector3u): Boolean {
        return x <= other.x && y <= other.y && z <= other.z
    }

//	fun addDimension(w: Float): Vector4i {
//		return Vector4i(x, y, z, w)
//	}


    fun toVector3f(): Vector3f {
        return Vector3f(x.toFloat(), y.toFloat(), z.toFloat())
    }

    fun toVector3d(): Vector3d {
        return Vector3d(x.toDouble(), y.toDouble(), z.toDouble())
    }

    fun toVector3i(): Vector3i {
        return Vector3i(x.toInt(), y.toInt(), z.toInt())
    }

    fun toVector3u(): Vector3u {
        return this
    }


    fun normalize(): Vector3u {
        return toVector3f().normalize().roundToVector3i().toVector3u()
    }

//    fun getXY(): Vector2i {
//        return Vector2i(x, y)
//    }
//
//    fun getYZ(): Vector2i {
//        return Vector2i(y, z)
//    }
//
//    fun getXZ(): Vector2i {
//        return Vector2i(x, z)
//    }
//
//    fun expand(w: UInt): Vector4i {
//        return Vector4i(x, y, z, w)
//    }

    fun half(): Vector3u {
        return this  / 2u
    }
    override fun toString(): String {
        return "V3i($x,$y,$z)"
    }


    fun component(index: UInt): UInt {
        return when (index) {
            0u -> x
            1u -> y
            2u -> z
            else -> throw Error("Only 3 components enabled")
        }
    }
}


