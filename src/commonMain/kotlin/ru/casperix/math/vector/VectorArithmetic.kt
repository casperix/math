package ru.casperix.math.vector

interface VectorArithmetic<Self : Any, Item:Number > {
    operator fun unaryMinus(): Self
    operator fun plus(position: Self): Self
    operator fun minus(position: Self): Self
    operator fun div(value: Self): Self
    operator fun div(value: Item): Self
    operator fun times(value: Self): Self
    operator fun times(value: Item): Self
    fun distTo(other: Self): Item
    fun length(): Item
    fun lengthOne(): Item
    fun lengthInf(): Item
    fun lengthSquared(): Item
}

