package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.vector.float32.Vector2f

/**
 * 	Axis aligned direction
 */
enum class Axis2f(val index: Int, val value: Vector2f) {
	NEGATIVE_X(0, -Vector2f.X),
	POSITIVE_X(1, Vector2f.X),
	NEGATIVE_Y(2, -Vector2f.Y),
	POSITIVE_Y(3, Vector2f.Y);

	fun invert(): Axis2f {
		return when (this) {
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}