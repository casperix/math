package ru.casperix.math.axis_aligned.float32

import ru.casperix.math.axis_aligned.Dimension2
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.format.FormatType
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable
import kotlin.math.roundToInt

@Serializable
data class Dimension2f(override val width: Float, override val height: Float) : Dimension2<Float> {

    fun roundToVector2i(): Vector2i {
        return Vector2i(width.roundToInt(), height.roundToInt())
    }

    fun roundToDimension2i(): Dimension2i {
        return Dimension2i(width.roundToInt(), height.roundToInt())
    }
    fun toVector2f(): Vector2f {
        return Vector2f(width, height)
    }

    override fun toString(): String {
        return format()
    }

    fun format(type: FormatType = FormatType.NORMAL, precision: Int = 2): String {
        return when (type) {
            FormatType.DETAIL -> "Dimension2f(width=${width.toPrecision(precision)}, height=${height.toPrecision(precision)})"
            FormatType.NORMAL -> "Dim2f(${width.toPrecision(precision)}, ${height.toPrecision(precision)})"
            FormatType.SHORT -> "${width.toPrecision(precision)}x${height.toPrecision(precision)}"
        }
    }

    companion object {
        val ZERO = Dimension2f(0f, 0f)
    }
}