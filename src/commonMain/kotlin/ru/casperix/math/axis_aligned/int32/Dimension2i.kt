package ru.casperix.math.axis_aligned.int32

import ru.casperix.math.axis_aligned.Dimension2
import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.format.FormatType
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable

@Serializable
data class Dimension2i(override val width: Int, override val height: Int) : Dimension2<Int> {
    fun toVector2i(): Vector2i {
        return Vector2i(width, height)
    }

    fun toVector2f(): Vector2f {
        return Vector2f(width.toFloat(), height.toFloat())
    }

    fun toDimension2f(): Dimension2f {
        return Dimension2f(width.toFloat(), height.toFloat())
    }

    fun volume():Int {
        return width * height
    }

    fun expand(depth:Int): Dimension3i {
        return Dimension3i(width, height, depth)
    }

    override fun toString(): String {
        return format()
    }

    fun format(type: FormatType = FormatType.NORMAL): String {
        return when (type) {
            FormatType.DETAIL -> "Dimension2i(width=${width}, height=${height})"
            FormatType.NORMAL -> "Dim2i(${width}, ${height})"
            FormatType.SHORT -> "${width}x${height}"
        }
    }

    companion object {
        val ZERO = Dimension2i(0, 0)
    }
}