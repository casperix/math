package ru.casperix.math.axis_aligned.int32

import ru.casperix.math.axis_aligned.Dimension3
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.int32.Vector3i
import kotlinx.serialization.Serializable

@Serializable
data class Dimension3i(override val width: Int, override val height: Int, override val depth: Int) : Dimension3<Int> {
    fun toVector3i(): Vector3i {
        return Vector3i(width, height, depth)
    }

    fun toVector3f(): Vector3f {
        return Vector3f(width.toFloat(), height.toFloat(), depth.toFloat())
    }

    fun volume(): Int {
        return width * height * depth
    }

    fun get2D(): Dimension2i {
        return Dimension2i(width, height)
    }

    companion object {
        val ZERO = Dimension3i(0, 0, 0)
    }
}