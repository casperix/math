package ru.casperix.math.axis_aligned.int32

import ru.casperix.math.vector.int32.Vector3i


/**
 * 	Axis aligned direction
 */
enum class Axis3i(val index: Int, val value: Vector3i) {
	NEGATIVE_X(0, -Vector3i.X),
	POSITIVE_X(1, Vector3i.X),
	NEGATIVE_Y(2, -Vector3i.Y),
	POSITIVE_Y(3, Vector3i.Y),
	NEGATIVE_Z(4, -Vector3i.Z),
	POSITIVE_Z(5, Vector3i.Z);

	fun invert(): Axis3i {
		return when (this) {
			NEGATIVE_Z -> POSITIVE_Z
			POSITIVE_Z -> NEGATIVE_Z
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}