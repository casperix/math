package ru.casperix.math.axis_aligned.float64

import ru.casperix.math.vector.float64.Vector3d


/**
 * 	Axis aligned direction
 */
enum class Axis3d(val index: Int, val value: Vector3d) {
	NEGATIVE_X(0, -Vector3d.X),
	POSITIVE_X(1, Vector3d.X),
	NEGATIVE_Y(2, -Vector3d.Y),
	POSITIVE_Y(3, Vector3d.Y),
	NEGATIVE_Z(4, -Vector3d.Z),
	POSITIVE_Z(5, Vector3d.Z);

	fun invert(): Axis3d {
		return when (this) {
			NEGATIVE_Z -> POSITIVE_Z
			POSITIVE_Z -> NEGATIVE_Z
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}