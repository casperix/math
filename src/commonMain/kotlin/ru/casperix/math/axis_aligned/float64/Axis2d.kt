package ru.casperix.math.axis_aligned.float64

import ru.casperix.math.vector.float64.Vector2d

/**
 * 	Axis aligned direction
 */
enum class Axis2d(val index: Int, val value: Vector2d) {
	NEGATIVE_X(0, -Vector2d.X),
	POSITIVE_X(1, Vector2d.X),
	NEGATIVE_Y(2, -Vector2d.Y),
	POSITIVE_Y(3, Vector2d.Y);

	fun invert(): Axis2d {
		return when (this) {
			NEGATIVE_Y -> POSITIVE_Y
			POSITIVE_Y -> NEGATIVE_Y
			NEGATIVE_X -> POSITIVE_X
			POSITIVE_X -> NEGATIVE_X
		}
	}
}