package ru.casperix.math.collection


class DoublyLinkedList<Value> {
    private var head: Node<Value>? = null
    private var tail: Node<Value>? = null
    private var counter = 0

    val size get() = counter

    /**
     * @param owner not necessary. but uses for check operations (for sample removing not self nodes)
     */
    class Node<Value>(val data: Value) {
        internal var owner: DoublyLinkedList<Value>? = null
        internal var prev: Node<Value>? = null
        internal var next: Node<Value>? = null

        internal fun grab(list: DoublyLinkedList<Value>) {
            require( checkOwner(null))
            list.counter++
            owner = list
        }

        internal fun drop(list: DoublyLinkedList<Value>) {
            require( checkOwner(list))
            list.counter--
            owner = null
        }

        internal fun checkOwner(list: DoublyLinkedList<Value>?) = (owner == list)

    }

    class ValueIterator<Value>(head: Node<Value>?) : Iterator<Value> {
        private var current = head
        override fun hasNext() = (current != null)
        override fun next() = current!!.data.apply {
            current = current!!.next
        }
    }

    class NodeIterator<Value>(head: Node<Value>?) : Iterator<Node<Value>> {
        private var current = head
        override fun hasNext() = (current != null)
        override fun next() = current!!.apply {
            current = current!!.next
        }
    }

    fun pushBack(data: Value): Node<Value> {
        val node = Node(data)
        node.grab(this)

        if (tail == null) {
            head = node
            tail = node
        } else {
            node.prev = tail
            tail!!.next = node
            tail = node
        }
        return node
    }

    fun moveFront(node: Node<Value>) {
        if (!node.checkOwner(this)) return

        remove(node)
        node.grab(this)

        if (head == null) {
            node.next = null
            node.prev = null

            head = node
            tail = node
        } else {
            node.prev = null
            node.next = head

            head?.prev = node
            head = node
        }
    }

    fun moveBack(node: Node<Value>) {
        if (!node.checkOwner(this)) return

        remove(node)
        node.grab(this)

        if (tail == null) {
            node.next = null
            node.prev = null

            head = node
            tail = node
        } else {
            node.next = null
            node.prev = tail

            tail!!.next = node
            tail = node
        }
    }

    fun pushFront(data: Value): Node<Value> {
        val node = Node(data)
        node.grab(this)

        if (head == null) {
            head = node
            tail = node
        } else {
            node.next = head

            head?.prev = node
            head = node
        }
        return node
    }

    fun search(data: Value) = nodes().firstOrNull { it.data == data }

    fun remove(data: Value):Boolean {
        search(data)?.let {
            return remove(it)
        }
        return false
    }

    fun removeFirst() :Node<Value> {
        val first = head!!
        first.drop(this)

        if (head == tail) {
            head = null
            tail = null
        } else {
            head = head!!.next
            head?.prev = null
        }

        return first
    }

    fun removeLast():Node<Value> {
        val last = tail!!
        last.drop(this)

        if (head == tail) {
            head = null
            tail = null
        } else {
            tail = tail!!.prev
            tail?.next = null
        }

        return last
    }

    fun remove(node: Node<Value>):Boolean {
        if (!node.checkOwner(this)) return false

        when (node) {
            head -> removeFirst()

            tail -> removeLast()

            else -> {
                node.drop(this)

                node.prev?.next = node.next
                node.next?.prev = node.prev
            }
        }
        return true
    }

    fun nodes(): Iterable<Node<Value>> = object : Iterable<Node<Value>> {
        override fun iterator() = NodeIterator(head)
    }

    fun values(): Iterable<Value> = object : Iterable<Value> {
        override fun iterator() = ValueIterator(head)
    }
}


