package ru.casperix.math.quad_matrix

interface QuadMatrixBuilder<Self, Basis, Angle> {
    fun translate(value: Basis): Self
    fun scale(value: Basis): Self

    /**
     * Counter-clockwise rotate matrix in RHS
     */
    fun rotate(angle: Angle): Self
}