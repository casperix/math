package ru.casperix.math.quaternion

import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.quaternion.float64.QuaternionDouble

fun quaternionOf(x: Float, y: Float, z: Float, w: Float): QuaternionFloat {
    return QuaternionFloat(x, y, z, w)
}

fun quaternionOf(x: Double, y: Double, z: Double, w: Double): QuaternionDouble {
    return QuaternionDouble(x, y, z, w)
}
