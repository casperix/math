package ru.casperix.math.transform.float32

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable

@Serializable
data class Transform2f(
    val position: Vector2f,
    val scale: Vector2f = Vector2f.ONE,
    val rotation: ru.casperix.math.angle.float32.DegreeFloat = ru.casperix.math.angle.float32.DegreeFloat.ZERO,
) {
    val matrix =Matrix3f.rotate(rotation) *  Matrix3f.translate(position) * Matrix3f.scale(scale)

    fun rotate(delta: ru.casperix.math.angle.float32.DegreeFloat): Transform2f {
        return copy(rotation = rotation + delta)
    }

    fun translate(delta: Vector2f): Transform2f {
        return copy(position = position + delta)
    }

    fun scale(delta: Vector2f): Transform2f {
        return copy(scale = scale + delta)
    }

    companion object {
        val ZERO = Transform2f(Vector2f.ZERO)
    }
}