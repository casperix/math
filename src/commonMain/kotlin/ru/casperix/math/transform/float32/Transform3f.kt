package ru.casperix.math.transform.float32

import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.interpolation.float32.InterpolateFloatFunction
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.interpolation.float32.linearInterpolatef
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.vector.float32.Vector3f
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class Transform3f(val position: Vector3f = Vector3f.ZERO, val scale: Vector3f = Vector3f.ONE, val rotation: QuaternionFloat = QuaternionFloat.IDENTITY) {
	@Transient
	private var cacheMatrix: Matrix4f? = null

	fun getMatrix(): Matrix4f {
		cacheMatrix?.let {
			return it
		}
		val result = Matrix4f.compose(position, scale, rotation)
		cacheMatrix = result
		return result
	}


	fun getLocalX(): Vector3f {
		return getLocalX(rotation)
	}

	fun getLocalY(): Vector3f {
		return getLocalY(rotation)
	}

	fun getLocalZ(): Vector3f {
		return getLocalZ(rotation)
	}

	fun isFinite(): Boolean {
		return rotation.isFinite() && position.isFinite() && scale.isFinite()
	}

	companion object {
		val IDENTITY = Transform3f(Vector3f.ZERO, Vector3f.ONE, QuaternionFloat.IDENTITY)

		fun interpolate(A: Transform3f, B: Transform3f, factor: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): Transform3f {
			val pos = InterpolationFloat.vector3(A.position, B.position, factor, interpolator)
			val scale = InterpolationFloat.vector3(A.scale, B.scale, factor, interpolator)
			val Q = InterpolationFloat.quaternion(A.rotation, B.rotation, factor)
			return Transform3f(pos, scale, Q)
		}

		fun getLocalX(orientation: QuaternionFloat): Vector3f {
			return orientation.transform(Vector3f.X).normalize()
		}

		fun getLocalY(orientation: QuaternionFloat): Vector3f {
			return orientation.transform(Vector3f.Y).normalize()
		}

		fun getLocalZ(orientation: QuaternionFloat): Vector3f {
			return orientation.transform(Vector3f.Z).normalize()
		}

		fun fromYAxis(position: Vector3f, yAxis: Vector3f, zAxis: Vector3f): Transform3f {
			val yAxisN = yAxis.normalize()
			val xAxisN = yAxis.cross(zAxis).normalize()
			val zAxisN = xAxisN.cross(yAxisN)
			return Transform3f(position, Vector3f.ONE, QuaternionFloat.fromAxis(xAxisN, yAxisN, zAxisN))
		}

		fun fromXAxis(position: Vector3f, xAxis: Vector3f, zAxis: Vector3f): Transform3f {
			val xAxisN = xAxis.normalize()
			val yAxisN = zAxis.cross(xAxis).normalize()
			val zAxisN = xAxisN.cross(yAxisN)
			return Transform3f(position, Vector3f.ONE, QuaternionFloat.fromAxis(xAxisN, yAxisN, zAxisN))
		}

		fun fromZAxis(position: Vector3f, zAxis: Vector3f, yAxis: Vector3f): Transform3f {
			val zAxisN = zAxis.normalize()
			val xAxisN = yAxis.cross(zAxis).normalize()
			val yAxisN = zAxisN.cross(xAxisN)
			return Transform3f(position, Vector3f.ONE, QuaternionFloat.fromAxis(xAxisN, yAxisN, zAxisN))
		}
	}
}
