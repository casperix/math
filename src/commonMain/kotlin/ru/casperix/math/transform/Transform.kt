package ru.casperix.math.transform

import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.transform.float32.Transform3f
import ru.casperix.math.transform.float64.Transform3d
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector3d


fun transformOf(position: Vector3f, scale: Vector3f = Vector3f.ONE, rotation: QuaternionFloat = QuaternionFloat.IDENTITY): Transform3f {
    return Transform3f(position, scale, rotation)
}

fun transformOf(position: Vector3d, scale: Vector3d = Vector3d.ONE, rotation: QuaternionDouble = QuaternionDouble.IDENTITY): Transform3d {
    return Transform3d(position, scale, rotation)
}