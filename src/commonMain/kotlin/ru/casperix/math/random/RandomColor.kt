package ru.casperix.math.random

import ru.casperix.math.color.rgb.RgbColor3d
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgba.RgbaColor4d
import ru.casperix.math.color.rgba.RgbaColor4f
import kotlin.random.Random

fun Random.nextColor4f(a: RgbaColor4f, b: RgbaColor4f): RgbaColor4f {
    return RgbaColor4f(
        nextFloat(a.red, b.red),
        nextFloat(a.green, b.green),
        nextFloat(a.blue, b.blue),
        nextFloat(a.alpha, b.alpha),
    )
}
fun Random.nextColor3f(a: RgbColor3f, b: RgbColor3f): RgbColor3f {
    return RgbColor3f(
        nextFloat(a.red, b.red),
        nextFloat(a.green, b.green),
        nextFloat(a.blue, b.blue),
    )
}
fun Random.nextColor4d(a: RgbaColor4d, b: RgbaColor4d): RgbaColor4d {
    return RgbaColor4d(
        nextDouble(a.red, b.red),
        nextDouble(a.green, b.green),
        nextDouble(a.blue, b.blue),
        nextDouble(a.alpha, b.alpha),
    )
}
fun Random.nextColor3d(a: RgbColor3d, b: RgbColor3d): RgbColor3d {
    return RgbColor3d(
        nextDouble(a.red, b.red),
        nextDouble(a.green, b.green),
        nextDouble(a.blue, b.blue),
    )
}

fun Random.nextColor3d(): RgbColor3d {
    return RgbColor3d(nextDouble(), nextDouble(), nextDouble())
}

fun Random.nextColor3d(red: ClosedRange<Double>, green: ClosedRange<Double>, blue: ClosedRange<Double>): RgbColor3d {
    return RgbColor3d(nextDouble(red), nextDouble(green), nextDouble(blue))
}

fun Random.nextColor3f(): RgbColor3f {
    return RgbColor3f(nextFloat(), nextFloat(), nextFloat())
}

fun Random.nextColor3f(red: ClosedRange<Float>, green: ClosedRange<Float>, blue: ClosedRange<Float>): RgbColor3f {
    return RgbColor3f(nextFloat(red), nextFloat(green), nextFloat(blue))
}

fun Random.nextColor4d(): RgbaColor4d {
    return RgbaColor4d(nextDouble(), nextDouble(), nextDouble(), nextDouble())
}

fun Random.nextColor4d(
    red: ClosedRange<Double>,
    green: ClosedRange<Double>,
    blue: ClosedRange<Double>,
    alpha: ClosedRange<Double>
): RgbaColor4d {
    return RgbaColor4d(nextDouble(red), nextDouble(green), nextDouble(blue), nextDouble(alpha))
}

fun Random.nextColor4f(): RgbaColor4f {
    return RgbaColor4f(nextFloat(), nextFloat(), nextFloat(), nextFloat())
}

fun Random.nextColor4f(red: ClosedRange<Float>, green: ClosedRange<Float>, blue: ClosedRange<Float>, alpha: ClosedRange<Float>): RgbaColor4f {
    return RgbaColor4f(nextFloat(red), nextFloat(green), nextFloat(blue), nextFloat(alpha))
}
