package ru.casperix.math.random

import kotlin.random.Random


fun Random.nextDouble(range: ClosedRange<Double>): Double {
    return nextDouble(range.start, range.endInclusive)
}
