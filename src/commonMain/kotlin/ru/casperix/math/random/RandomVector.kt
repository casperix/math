package ru.casperix.math.random

import ru.casperix.math.axis_aligned.float64.Box2d
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i
import kotlin.random.Random


fun Random.nextVector2i(min: Int, max: Int): Vector2i {
    return nextVector2i(Vector2i(min), Vector2i(max))
}

fun Random.nextVector2i(range: IntRange): Vector2i {
    return nextVector2i(Vector2i(range.first), Vector2i(range.last))
}

fun Random.nextVector2i(): Vector2i {
    return nextVector2i(Vector2i.ONE)
}

fun Random.nextVector2i(until: Vector2i): Vector2i {
    return Vector2i(nextInt(until.x), nextInt(until.y))
}

fun Random.nextVector2i(from: Vector2i, to: Vector2i): Vector2i {
    return Vector2i(nextInt(from.x, to.x), nextInt(from.y, to.y))
}


fun Random.nextVector3i(): Vector3i {
    return nextVector3i(Vector3i.ONE)
}

fun Random.nextVector3i(until: Vector3i): Vector3i {
    return Vector3i(nextInt(until.x), nextInt(until.y), nextInt(until.z))
}

fun Random.nextVector3i(from: Vector3i, to: Vector3i): Vector3i {
    return Vector3i(nextInt(from.x, to.x), nextInt(from.y, to.y), nextInt(from.z, to.z))
}


fun Random.nextVector3d(): Vector3d {
    return Vector3d(nextDouble(), nextDouble(), nextDouble())
}

fun Random.nextVector3d(until: Vector3d): Vector3d {
    return Vector3d(nextDouble(until.x), nextDouble(until.y), nextDouble(until.z))
}

fun Random.nextVector3d(from: Vector3d, to: Vector3d): Vector3d {
    return Vector3d(nextDouble(from.x, to.x), nextDouble(from.y, to.y), nextDouble(from.z, to.z))
}


fun Random.nextVector2d(): Vector2d {
    return Vector2d(nextDouble(), nextDouble())
}

fun Random.nextVector2d(until: Vector2d): Vector2d {
    return Vector2d(nextDouble(until.x), nextDouble(until.y))
}

fun Random.nextVector2d(from: Vector2d, to: Vector2d): Vector2d {
    return Vector2d(nextDouble(from.x, to.x), nextDouble(from.y, to.y))
}

fun Random.nextVector2d(source: Box2d): Vector2d {
    return source.min + nextVector2d(source.dimension)
}


fun Random.nextVector3f(until: Vector3f): Vector3f {
    return Vector3f(nextFloat(until.x), nextFloat(until.y), nextFloat(until.z))
}

fun Random.nextVector3f(from: Vector3f, to: Vector3f): Vector3f {
    return Vector3f(nextFloat(from.x, to.x), nextFloat(from.y, to.y), nextFloat(from.z, to.z))
}

fun Random.nextVector3f(): Vector3f {
    return Vector3f(nextFloat(), nextFloat(), nextFloat())
}


fun Random.nextVector2f(until: Vector2f): Vector2f {
    return Vector2f(nextFloat(until.x), nextFloat(until.y))
}

fun Random.nextVector2f(from: Vector2f, to: Vector2f): Vector2f {
    return Vector2f(nextFloat(from.x, to.x), nextFloat(from.y, to.y))
}

fun Random.nextVector2f(): Vector2f {
    return Vector2f(nextFloat(), nextFloat())
}

fun Random.nextVector2f(source: Box2f): Vector2f {
    return source.min + nextVector2f(source.dimension)
}

fun Random.nextVector2f(min: Float, max: Float): Vector2f {
    return nextVector2f(Vector2f(min), Vector2f(max))
}
