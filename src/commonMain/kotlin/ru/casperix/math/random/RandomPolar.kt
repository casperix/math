package ru.casperix.math.random

import ru.casperix.math.geometry.PI2
import ru.casperix.math.geometry.fPI2
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.polar.float64.PolarCoordinateDouble
import kotlin.random.Random

fun Random.nextPolarCoordinateFloat(
    minRange: Float = 0f,
    maxRange: Float = 1f,
    minAngle: Float = 0f,
    maxAngle: Float = fPI2
): PolarCoordinateFloat {
    return PolarCoordinateFloat(nextFloat(minRange, maxRange), nextRadianFloat(minAngle, maxAngle))
}

fun Random.nextPolarCoordinateDouble(
    minRange: Double = 0.0,
    maxRange: Double = 1.0,
    minAngle: Double = 0.0,
    maxAngle: Double = PI2
): PolarCoordinateDouble {
    return PolarCoordinateDouble(nextDouble(minRange, maxRange), nextRadianDouble(minAngle, maxAngle))
}
