package ru.casperix.math.camera

import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable

@Serializable
data class OrthographicCameraStateOld(val zoom:Float = 1f, val position: Vector2f = Vector2f.ZERO)