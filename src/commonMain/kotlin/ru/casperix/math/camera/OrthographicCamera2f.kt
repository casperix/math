package ru.casperix.math.camera

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f

class OrthographicCamera2f(
    initialZoom: Float,
    initialPosition: Vector2f,
    initialViewport: Dimension2f,
    val yDown: Boolean = false,
    val centered: Boolean = false
) {
    var transform: CameraTransform2f = calculate(initialViewport, initialZoom, initialPosition, yDown, centered); private set

    var zoom = initialZoom
        set(value) {
            if (field == value) return
            field = value
            invalidate()
        }
    var position = initialPosition
        set(value) {
            if (field == value) return
            field = value
            invalidate()
        }
    var viewport = initialViewport
        set(value) {
            if (field == value) return
            field = value
            invalidate()
        }

    private fun invalidate() {
        transform = calculate(viewport, zoom, position, yDown, centered)
    }

    companion object {
        fun calculate(
            viewport: Dimension2f,
            zoom: Float,
            position: Vector2f,
            yDown: Boolean = false,
            centered: Boolean = true,
        ): CameraTransform2f = viewport.run {
            val yFactor = if (yDown) -1f else 1f

            val projection = if (centered) {
                Matrix3f.orthographic(
                    -width / 2f / zoom,
                    width / 2f / zoom,
                    -height / 2f / zoom * yFactor,
                    height / 2f / zoom * yFactor,
                )
            } else {
                if (yDown) {
                    Matrix3f.orthographic(
                        0f,
                        width / zoom,
                        height / zoom,
                        0f,
                    )
                } else {
                    Matrix3f.orthographic(
                        0f,
                        width / zoom,
                        0f,
                        height / zoom,
                    )
                }
            }

            val view = Matrix3f.translate(-position)
            CameraTransform2f(projection, view, viewport)
        }
    }
}