package ru.casperix.math.array.uint8

import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.vector.int32.Vector2i

@OptIn(ExperimentalUnsignedTypes::class)
class UByteBasedMap2D<Custom : Any>(val bytes: UByteArray3D, val codec: UByteCodec<Custom>) : MutableMap2D<Custom> {
	override val dimension = bytes.dimension.getXY()

	override fun get(position: Vector2i): Custom {
		val pixel = bytes.getColumn(position)
		return codec.decode(pixel)
	}

	override fun set(position: Vector2i, value: Custom) {
		val pixel = codec.encode(value)
		bytes.setColumn(position, pixel)
	}
}