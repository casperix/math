package ru.casperix.math.array.uint8

interface UByteCodec<Custom> {
	fun decode(bytes: UByteArray): Custom
	fun encode(custom: Custom): UByteArray
}