package ru.casperix.math.array.int8

import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.vector.int32.Vector2i

class ByteBasedMap2D<Custom : Any>(val bytes: ByteArray3D, val codec: ByteCodec<Custom>) : MutableMap2D<Custom> {
	override val dimension = bytes.dimension.getXY()

	override fun get(position: Vector2i): Custom {
		val pixel = bytes.getColumn(position)
		return codec.decode(pixel)
	}

	override fun set(position: Vector2i, value: Custom) {
		val pixel = codec.encode(value)
		bytes.setColumn(position, pixel)
	}
}