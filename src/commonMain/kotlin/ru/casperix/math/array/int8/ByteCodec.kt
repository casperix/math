package ru.casperix.math.array.int8

interface ByteCodec<Custom> {
	fun decode(bytes: ByteArray): Custom
	fun encode(custom: Custom): ByteArray
}