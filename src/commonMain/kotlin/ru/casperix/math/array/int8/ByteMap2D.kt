package ru.casperix.math.array.int8

import ru.casperix.math.array.IndexedMap2D
import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i
import kotlinx.serialization.Serializable

@Serializable
data class ByteMap2D(override val dimension: Vector2i, val array: ByteArray) : IndexedMap2D<Byte>, MutableMap2D<Byte> {

	override fun setByIndex(index: Int, value: Byte) {
		array[index] = value
	}

	override fun getByIndex(index: Int): Byte {
		return array[index]
	}


	companion object {
		fun create(dimension: Vector2i, builder: (index: Int) -> Byte): ByteMap2D {
			return ByteMap2D(dimension, ByteArray(dimension.volume()) { builder(it) })
		}
	}

	init {
		if (array.size != dimension.volume()) throw Error("Invalid array size. Need: ${dimension.volume()}")
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other == null || this::class != other::class) return false

		other as ByteMap2D

		if (!array.contentEquals(other.array)) return false
		if (dimension != other.dimension) return false

		return true
	}

	override fun hashCode(): Int {
		var result = array.contentHashCode()
		result = 31 * result + dimension.hashCode()
		return result
	}


}