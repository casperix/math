package ru.casperix.math.array

import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i

object ArrayAccessND  {
    fun index3D(dimension: Vector3i, position: Vector3i): Int {
        if (position.less(Vector3i.ZERO) || position.greaterOrEq(dimension.toVector3i())) {
            throw Error("Position $position is outside dimension ($dimension)")
        }
        return (position.x + dimension.x * position.y) * dimension.z + position.z
    }

    fun index2D(dimension: Vector2i, position: Vector2i): Int {
        if (position.less(Vector2i.ZERO) || position.greaterOrEq(dimension.toVector2i())) {
            throw Error("Position $position is outside dimension ($dimension)")
        }
        return position.x + dimension.x * position.y
    }

    fun position2D(dimension: Vector2i, index: Int): Vector2i {
        if (index < 0 || index >= dimension.volume()) {
            throw Error("Index $index is outside dimension ($dimension)")
        }
        val x = index % dimension.x
        val y = index / dimension.x
        return Vector2i(x, y)
    }

}