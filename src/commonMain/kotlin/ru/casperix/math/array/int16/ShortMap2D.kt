package ru.casperix.math.array.int16

import ru.casperix.math.array.ArrayAccessND
import ru.casperix.math.array.IndexedMap2D
import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.vector.int32.Vector2i
import kotlinx.serialization.Serializable

@Serializable
data class ShortMap2D(
    override val dimension: Vector2i, /*@ProtoType(ProtoIntegerType.SIGNED) */val array: ShortArray
) : IndexedMap2D<Short>, MutableMap2D<Short> {

    override fun setByIndex(index: Int, value: Short) {
        array[index] = value
    }

    override fun getByIndex(index: Int): Short {
        return array[index]
    }

    companion object {
        fun create(dimension: Vector2i, builder: (index: Int) -> Short): ShortMap2D {
            return ShortMap2D(dimension, ShortArray(dimension.volume()) { builder(it) })
        }

        fun createByXY(dimension: Vector2i, builder: (pos: Vector2i) -> Short): ShortMap2D {
            return ShortMap2D(dimension, ShortArray(dimension.volume()) {
                val pos = ArrayAccessND.position2D(dimension, it)
                builder(pos)
            })
        }
    }

    init {
        if (array.size != dimension.volume()) throw Error("Invalid array size. Need: ${dimension.volume()}")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as ShortMap2D

        if (!array.contentEquals(other.array)) return false
        if (dimension != other.dimension) return false

        return true
    }

    override fun hashCode(): Int {
        var result = array.contentHashCode()
        result = 31 * result + dimension.hashCode()
        return result
    }


}