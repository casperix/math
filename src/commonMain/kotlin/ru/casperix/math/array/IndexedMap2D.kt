package ru.casperix.math.array

import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i

/**
 * representation of each cell of a two-dimensional map by a specific linear array index
 */
interface IndexedMap2D<Cell : Any> : MutableMap2D<Cell> {
	override val dimension: Vector2i

	fun setByIndex(index: Int, value: Cell)

	fun getByIndex(index: Int): Cell


	override fun get(position: Vector2i): Cell {
		return getByIndex(ArrayAccessND.index2D(dimension, position))
	}

	override fun set(position: Vector2i, value: Cell) {
		return setByIndex(ArrayAccessND.index2D(dimension, position), value)
	}

}
