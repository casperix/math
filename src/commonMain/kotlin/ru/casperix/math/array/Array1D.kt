package ru.casperix.math.array

interface Array1D<Item : Any> {
    val size:Int
    operator fun get(index: Int): Item
    operator fun iterator(): Iterator<Item>
}

