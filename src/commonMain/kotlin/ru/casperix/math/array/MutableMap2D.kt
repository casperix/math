package ru.casperix.math.array

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i

interface MutableMap2D<Cell:Any> : Map2D<Cell> {

	fun set(position: Vector2i, value: Cell)

	fun set(x: Int, y: Int, value: Cell)  {
		set(Vector2i(x, y), value)
	}

	fun set(position: Vector2i, other: Map2D<Cell>, otherArea: Box2i) {
		otherArea.iterator().forEach { sourcePos ->
			other.getOrNull(sourcePos)?.let { value ->
				val targetPos = position + sourcePos - otherArea.min
				if (isInside(targetPos)) {
					set(targetPos, value)
				}
			}
		}
	}

}