package ru.casperix.math.array.float32

interface FloatCodec<Custom> {
	fun decode(bytes: FloatArray): Custom
	fun encode(custom: Custom): FloatArray

	val floatsPerRecord:Int
}