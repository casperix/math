package ru.casperix.math.array.float32

import kotlin.math.max

class FloatArrayBuilder(initialSize: Int = 16) : ArrayBuilder<FloatArray> {
     private var usage: Int = 0
    private var buffer = FloatArray(initialSize)

    override fun append(data: FloatArray) {
        val end = usage
        length = end + data.size
        data.copyInto(buffer, end)
    }

    fun append(value: Float) {
        val end = usage
        length = end + 1
        buffer[end] = value
    }

    override var length: Int
        get() = usage
        set(value) {
            if (value > buffer.size) {
                val next = FloatArray(max(value, buffer.size * 2))
                buffer.copyInto(next)
                buffer = next
            }
            usage = value
        }

    override fun build(): FloatArray {
        return buffer.sliceArray(0 until usage)
    }
}