package ru.casperix.math.spherical.float32

import ru.casperix.math.interpolation.float32.InterpolateFloatFunction
import ru.casperix.math.interpolation.float32.linearInterpolatef
import ru.casperix.math.spherical.float64.SphericalCoordinateDouble
import ru.casperix.math.vector.float32.Vector3f
import kotlinx.serialization.Serializable
import kotlin.math.cos
import kotlin.math.sin

@Serializable
data class SphericalCoordinateFloat(val range: Float, val verticalAngle: Float, val horizontalAngle: Float) {
	fun fromSpherical(): Vector3f {
		val sinV = sin(verticalAngle)
		val cosV = cos(verticalAngle)
		val cosH = cos(horizontalAngle)
		val sinH = sin(horizontalAngle)
		return Vector3f(range * sinV * cosH, range * sinV * sinH, range * cosV)
	}

	fun asVector3f(): Vector3f {
		return Vector3f(range, verticalAngle, horizontalAngle)
	}

	fun toSphericalCoordinated(): SphericalCoordinateDouble {
		return SphericalCoordinateDouble(range.toDouble(), verticalAngle.toDouble(), horizontalAngle.toDouble())
	}

	fun interpolate(A: SphericalCoordinateFloat, B: SphericalCoordinateFloat, factor: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): SphericalCoordinateFloat {
		return SphericalCoordinateFloat(
			interpolator(A.range, B.range, factor),
			interpolator(A.verticalAngle, B.verticalAngle, factor),
			interpolator(A.horizontalAngle, B.horizontalAngle, factor)
		)
	}

	companion object {
		val ZERO = SphericalCoordinateFloat(0f, 0f, 0f)
	}
}