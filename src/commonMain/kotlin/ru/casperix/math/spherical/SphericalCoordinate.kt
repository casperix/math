package ru.casperix.math.spherical

import ru.casperix.math.spherical.float32.SphericalCoordinateFloat
import ru.casperix.math.spherical.float64.SphericalCoordinateDouble

fun sphericalCoordinateOf(range: Float, verticalAngle: Float, horizontalAngle: Float): SphericalCoordinateFloat {
    return SphericalCoordinateFloat(range, verticalAngle, horizontalAngle)
}

fun sphericalCoordinateOf(range: Double, verticalAngle: Double, horizontalAngle: Double): SphericalCoordinateDouble {
    return SphericalCoordinateDouble(range, verticalAngle, horizontalAngle)
}
