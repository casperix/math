package ru.casperix.math.function

import ru.casperix.misc.clamp
import ru.casperix.math.vector.float64.Vector2d

typealias Function1D = (Double) -> Double
typealias Function2D = (Double, Double) -> Double
typealias Function3D = (Double, Double, Double) -> Double

typealias Function2DTransform = (source: Function2D, x: Double, y: Double) -> Double

fun Function2D.addModifier(modifier: Function1D): Function2D {
	return { x, y ->
		modifier(this(x, y))
	}
}

fun Function2D.addModifier(modifier: Function2DTransform): Function2D {
	return { x, y ->
		modifier(this, x, y)
	}
}

fun Function2D.clamp(min: Double, max: Double): Function2D {
	return { x, y ->
		this(x, y).clamp(min, max)
	}
}

operator fun Function2D.plus(modifier: Function2DTransform): Function2D {
	return addModifier(modifier)
}

operator fun Function2D.plus(modifier: Function1D): Function2D {
	return addModifier(modifier)
}

operator fun Function2D.plus(value: Double): Function2D {
	return { x, y ->
		value + this(x, y)
	}
}

operator fun Function2D.times(value: Double): Function2D {
	return { x, y ->
		value * this(x, y)
	}
}


fun scaleArguments(scale: Vector2d): Function2DTransform {
	return { source, x, y ->
		source(x * scale.x, y * scale.y)
	}
}

fun translateArguments(offset: Vector2d): Function2DTransform {
	return { source, x, y ->
		source(x + offset.x, y + offset.y)
	}
}


fun scaleAndTranslateArguments(scale: Vector2d, translate: Vector2d): Function2DTransform {
	return { source, x, y ->
		source(x * scale.x + translate.x, y * scale.y + translate.y)
	}
}

