package ru.casperix.math.curve

import ru.casperix.math.curve.float32.ParametricCurve2f
import ru.casperix.math.vector.float32.Vector2f

data class CurvePositionEntry(val curve: ParametricCurve2f, val t:Float, val position: Vector2f)