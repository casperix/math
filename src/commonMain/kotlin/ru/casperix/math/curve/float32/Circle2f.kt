package ru.casperix.math.curve.float32

import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.geometry.fPI2
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable

/**
 * arc from startAngle to finishAngle (counter-clock-wise)
 */
@Serializable
data class Circle2f(val center: Vector2f, val range: Float) : ParametricCurve2f {
    override fun divide(t: Float): Pair<ParametricCurve2f, ParametricCurve2f> {
        return Pair(this, this)
    }

    override fun getPosition(t: Float): Vector2f {
        val angle = fPI2 * t
        return center + PolarCoordinateFloat.byRadian(range, angle).toDecart()
    }

    override fun length(): Float {
        return fPI2 * range
    }


    fun getParameterByPoint(point: Vector2f): Float {
        val angle = RadianFloat.byDirection(point - center).value
        return angle / fPI2
    }
}