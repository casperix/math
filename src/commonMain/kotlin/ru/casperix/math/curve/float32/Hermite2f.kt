package ru.casperix.math.curve.float32

import ru.casperix.math.array.float32.FloatMap2D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.curve.CurveHelper
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i

@Deprecated(message = "Use Bezier2f")
class Hermite2f(
    override val start: Vector2f,
    val startDirection: Vector2f,
    override val finish: Vector2f,
    val finishDirection: Vector2f
) :
    ParametricCurve2f {
    override fun length(): Float {
        return CurveHelper.calculateLength(this, 10)
    }

    override fun getPosition(t: Float): Vector2f {
        val T = FloatMap2D(Vector2i(4, 1), floatArrayOf(t * t * t, t * t, t, 1f))
        val R = (T * basis) ?: throw Exception("Invalid algorithm")
        return start * R.get(0, 0) +
                finish * R.get(1, 0) +
                startDirection * R.get(2, 0) * 3f +
                finishDirection * R.get(3, 0) * 3f
    }

    override fun divide(t: Float): Pair<ParametricCurve2f, ParametricCurve2f> {
        val p0 = start
        val p1 = start + startDirection
        val p2 = finish
        val p3 = finish - finishDirection
        return BezierCubic2f(p0, p1, p2, p3).divide(t)
    }

    companion object {
        val basis = FloatMap2D(
            Vector2i(4), floatArrayOf(
                2f, -2f, 1f, 1f,
                -3f, 3f, -2f, -1f,
                0f, 0f, 1f, 0f,
                1f, 0f, 0f, 0f,
            )
        )
    }
}