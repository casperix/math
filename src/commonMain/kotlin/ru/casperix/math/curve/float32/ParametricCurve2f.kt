package ru.casperix.math.curve.float32

import ru.casperix.math.curve.ParametricCurve
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.rotateCCW

typealias Curve2f = ParametricCurve2f

interface ParametricCurve2f : ParametricCurve<ParametricCurve2f, Float, Vector2f> {

    override fun getTangent(t: Float): Vector2f {
        val smallValue = 0.0001f
        return if (t <= smallValue) {
            (getPosition(t + smallValue) - getPosition(t)).normalize()
        } else if (t >= 1f - smallValue) {
            (getPosition(t) - getPosition(t - smallValue)).normalize()
        } else {
            (getPosition(t + smallValue) - getPosition(t - smallValue)).normalize()
        }
    }

    override fun getProjection(position: Vector2f): Float {
        return (0..10).map {
            val t = it / 10f
            Pair(t, getPosition(t).distTo(position))
        }.minByOrNull { (_, dist) -> dist }!!.first
    }

    override fun split(factors: List<Float>): List<ParametricCurve2f> {
        if (factors.isEmpty()) return listOf(this)

        var tOffset = 0f
        var source = this

        return factors.sorted().map { next ->
            val s = (next - tOffset) / (1f - tOffset)
            val (first, last) = source.divide(s)

            source = last
            tOffset = next
            first
        } + source
    }

    override fun getNormal(t: Float): Vector2f {
        return getTangent(t).rotateCCW()
    }

    override operator fun plus(other: ParametricCurve2f): ParametricCurve2f {
        return CurveUnion2f(this, other)
    }

    override val start get() = getPosition(0f)

    override val finish get() = getPosition(1f)

    override fun invert(): ParametricCurve2f {
        TODO()
    }

    override fun grow(startOffset: Float, finishOffset: Float): ParametricCurve2f {
        TODO()
    }

}


