package ru.casperix.math.curve.float32

import ru.casperix.math.curve.CurveHelper
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable
import kotlin.math.pow

@Serializable
data class BezierQuadratic2f(val p0: Vector2f, val p1: Vector2f, val p2: Vector2f) : ParametricCurve2f {

    override fun getPosition(t: Float): Vector2f {
        val s = 1f - t
        val k0 = s.pow(2)
        val k1 = 2f * s * t
        val k2 = t.pow(2)
        return Vector2f(
            p0.x * k0 + p1.x * k1 + p2.x * k2,
            p0.y * k0 + p1.y * k1 + p2.y * k2,
        )
    }

    override fun length(): Float {
        return CurveHelper.calculateLength(this, 10)
    }



    override fun divide(t: Float): Pair<BezierQuadratic2f, BezierQuadratic2f> {
        val AB = InterpolationFloat.vector2(p0, p1, t)
        val BC = InterpolationFloat.vector2(p1, p2, t)
        val H = InterpolationFloat.vector2(AB, BC, t)

        return Pair(BezierQuadratic2f(p0, AB, H), BezierQuadratic2f(H, BC, p2))
    }
}