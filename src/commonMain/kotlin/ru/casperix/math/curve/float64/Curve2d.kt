package ru.casperix.math.curve.float64

import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.rotateCCW

interface Curve2d {
    fun getTangent(t: Double): Vector2d

    fun invert(): Curve2d
    fun getProjection(position: Vector2d): Double

    fun split(factors: List<Double>): List<Curve2d> {
        if (factors.isEmpty()) return listOf(this)

        var tOffset = 0.0
        var source = this

        return factors.sorted().map { next ->
            val s = (next - tOffset) / (1f - tOffset)
            val (first, last) = source.divide(s)

            source = last
            tOffset = next
            first
        } + source
    }

    operator fun plus(other: Curve2d): Curve2d {
        return CurveUnion2d(this, other)
    }

    fun divide(t: Double): Pair<Curve2d, Curve2d>

    fun getNormal(t: Double): Vector2d {
        return getTangent(t).rotateCCW()
    }

    fun getPosition(t: Double): Vector2d
    fun length(): Double

    val start get() = getPosition(0.0)

    val finish get() = getPosition(1.0)

    fun grow(offset: Double): Curve2d {
        return grow(offset, offset)
    }

    fun grow(startOffset: Double, finishOffset: Double): Curve2d {
        TODO()
    }

}


