package ru.casperix.math.curve.float64

import ru.casperix.math.geometry.Line2d
import ru.casperix.math.geometry.delta
import ru.casperix.math.interpolation.float64.InterpolationDouble
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.rotateCCW
import kotlinx.serialization.Serializable

@Serializable
data class LineCurve2d(val line: Line2d) : Curve2d {
    override val start: Vector2d get() = line.v0
    override val finish: Vector2d get() = line.v1

    constructor(start: Vector2d, finish: Vector2d) : this(Line2d(start, finish))

    override fun getPosition(t: Double): Vector2d {
        return InterpolationDouble.vector2(start, finish, t)
    }

    override fun invert(): LineCurve2d {
        return LineCurve2d(finish, start)
    }

    override fun getTangent(t: Double): Vector2d {
        return line.delta().normalize()
    }

    override fun getNormal(t: Double): Vector2d {
        return getTangent(t).rotateCCW()
    }

    override fun length(): Double {
        return line.v0.distTo(line.v1)
    }

    override fun divide(t: Double): Pair<LineCurve2d, LineCurve2d> {
        val d = line.delta()
        return Pair(LineCurve2d(start, start + d * t), LineCurve2d(start + d * t, finish))

    }

    override fun getProjection(point: Vector2d): Double {
        val direction = line.delta().normalize()
        val projection = (point - start).dot(direction)
        return projection / length()
    }

    override fun grow(startOffset: Double, finishOffset: Double): Curve2d {
        val offset = line.delta().normalize()
        return LineCurve2d(start - offset * startOffset, finish + offset * finishOffset)
    }
}