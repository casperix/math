package ru.casperix.math.curve.float64

import ru.casperix.math.vector.float64.Vector2d
import kotlinx.serialization.Serializable


@Serializable
class CurveUnion2d(val first: Curve2d, val second: Curve2d) : LazyCurve2d() {
    private val firstLength = first.length()
    private val secondLength = second.length()
    private val length = firstLength + secondLength

    override fun length(): Double {
        return length
    }

    override fun invert(): CurveUnion2d {
        return CurveUnion2d(second.invert(), first.invert())
    }

    private fun getPart(t: Double): Pair<Curve2d, Double> {
        val position = t * length
        return if (position <= firstLength) {
            Pair(first, position / firstLength)
        } else {
            Pair(second, (position - firstLength) / secondLength)
        }

    }

    override fun divide(t: Double): Pair<Curve2d, Curve2d> {
        val (curve, s) = getPart(t)
        val (left, right) = curve.divide(s)
        return if (curve == first) {
            Pair(left, right + second)
        } else {
            Pair(first + left, right)
        }
    }

    override fun getPosition(t: Double): Vector2d {
        val (curve, s) = getPart(t)
        return curve.getPosition(s)
    }

    override fun getTangent(t: Double): Vector2d {
        val (curve, s) = getPart(t)
        return curve.getTangent(s)
    }

    override fun getNormal(t: Double): Vector2d {
        val (curve, s) = getPart(t)
        return curve.getNormal(s)
    }
}