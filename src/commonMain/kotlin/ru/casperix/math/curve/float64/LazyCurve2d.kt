package ru.casperix.math.curve.float64

import ru.casperix.math.vector.float64.Vector2d

abstract class LazyCurve2d : Curve2d {
    override fun getTangent(t: Double): Vector2d {
        val smallValue = 0.0001
        if (t <= smallValue) {
            return (getPosition(t + smallValue) - getPosition(t)).normalize()
        } else if (t >= 1f - smallValue) {
            return (getPosition(t) - getPosition(t - smallValue)).normalize()
        } else {
            return (getPosition(t + smallValue) - getPosition(t - smallValue)).normalize()
        }
    }

    override fun invert(): Curve2d {
        TODO()
    }

    override fun getProjection(position: Vector2d): Double {
        return (0..10).map {
            val t = it / 10.0
            Pair(t, getPosition(t).distTo(position))
        }.minByOrNull { (_, dist)-> dist }!!.first
    }

}