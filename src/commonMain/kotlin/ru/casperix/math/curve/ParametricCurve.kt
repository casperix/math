package ru.casperix.math.curve

import ru.casperix.math.curve.float32.ParametricCurve2f
import ru.casperix.math.vector.api.AbstractVector

interface ParametricCurve<Self, Param : Number, Point : AbstractVector<Point>> {
    fun getPosition(t: Param): Point

    fun getTangent(t: Param): Point

    fun getNormal(t: Param): Point

    fun getProjection(position: Point): Float

    fun invert(): ParametricCurve2f

    fun split(factors: List<Param>): List<Self>

    fun divide(t: Param): Pair<Self, Self>
    fun length(): Float

    operator fun plus(other: Self): Self

    val start: Point

    val finish: Point

    fun grow(startOffset: Param, finishOffset: Param): Self

    fun grow(offset: Param): Self {
        return grow(offset, offset)
    }
}