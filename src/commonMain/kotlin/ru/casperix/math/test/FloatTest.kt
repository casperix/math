package ru.casperix.math.test

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.quad_matrix.float32.Matrix4f
import ru.casperix.math.test.FloatCompare.isLike
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import kotlin.test.assertTrue

object FloatTest {

    fun assertEquals(a: Float, b: Float) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: ru.casperix.math.angle.float32.DegreeFloat, b: ru.casperix.math.angle.float32.DegreeFloat) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: RadianFloat, b: RadianFloat) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: Vector2f, b: Vector2f) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: Vector3f, b: Vector3f) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: Matrix3f, b: Matrix3f) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

    fun assertEquals(a: Matrix4f, b: Matrix4f) {
        assertTrue(isLike(a, b), "$a is not $b")
    }

}

