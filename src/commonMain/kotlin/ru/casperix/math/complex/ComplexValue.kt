package ru.casperix.math.complex

import ru.casperix.math.complex.float32.ComplexValueFloat
import ru.casperix.math.complex.float64.ComplexValueDouble

fun complexValueOf(a: Float, b: Float): ComplexValueFloat {
    return ComplexValueFloat(a, b)
}

fun complexValueOf(a: Double, b: Double): ComplexValueDouble {
    return ComplexValueDouble(a, b)
}