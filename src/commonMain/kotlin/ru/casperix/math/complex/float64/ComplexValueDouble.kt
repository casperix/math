package ru.casperix.math.complex.float64

import ru.casperix.math.complex.float32.ComplexValueFloat
import kotlin.math.pow
import kotlin.math.sqrt

class ComplexValueDouble(val a: Double, val b: Double) {

	operator fun plus(other: ComplexValueDouble): ComplexValueDouble {
		return ComplexValueDouble(this.a + other.a, this.b + other.b)
	}

	operator fun minus(other: ComplexValueDouble): ComplexValueDouble {
		return ComplexValueDouble(this.a - other.a, this.b - other.b)
	}

	operator fun times(value: Double): ComplexValueDouble {
		return ComplexValueDouble(this.a * value, this.b * value)
	}

	operator fun times(other: ComplexValueDouble): ComplexValueDouble {
		return ComplexValueDouble(this.a * other.a - this.b * other.b, this.a * other.b + this.b * other.a)
	}

	fun conjugate(): ComplexValueDouble {
		return ComplexValueDouble(a, -b)
	}

	fun norm(): Double {
		return sqrt(a * a + b * b)
	}

	operator fun div(other: ComplexValueDouble): ComplexValueDouble {
		val d = other.a.pow(2) + other.b.pow(2)
		return ComplexValueDouble((this.a * other.a + this.b * other.b) / d, (this.b * other.a - this.a * other.b) / d)
	}

	fun toComplexValuef(): ComplexValueFloat {
		return ComplexValueFloat(a.toFloat(), b.toFloat())
	}
}