package ru.casperix.math.color

import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.rgb.RgbColor3f

/**
 * If it is important to work with physical color (in particular, without alpha), use this interface
 *
 * Otherwise see [ru.casperix.math.color.Color]
 */
interface PhysicColor : Color