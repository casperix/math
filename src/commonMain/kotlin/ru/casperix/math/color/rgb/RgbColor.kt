package ru.casperix.math.color.rgb

import ru.casperix.math.color.PhysicColor
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.misc.ColorDecoder
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.vector.float32.Vector3f

interface RgbColor : PhysicColor {

    fun toColor3b(): RgbColor3b
    fun toColor3d(): RgbColor3d
    fun toColor3f(): RgbColor3f

    fun toVector3f() = toColor3f().run { Vector3f(red, green, blue) }

    fun toHexString(): String = toColor3f().run {
        ColorDecoder.getColorComponentToHex(red) +
                ColorDecoder.getColorComponentToHex(green) +
                ColorDecoder.getColorComponentToHex(blue)
    }

    override fun toHSV() = toColor3f().toHSV()
    override fun toRGB() = toColor3f()
    override fun toRGBA() = toRGBA(1f)

    fun toRGBA(alpha: Float = 1f) = toColor3f().run { RgbaColor4f(red, green, blue, alpha) }

}