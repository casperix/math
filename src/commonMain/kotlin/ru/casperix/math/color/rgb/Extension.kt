package ru.casperix.math.color.rgb

import ru.casperix.math.color.rgba.RgbaColor4d
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.float64.Vector4d

fun Vector3d.toRGBColor() = RgbColor3d(x, y, z)
fun Vector3f.toRGBColor() = RgbColor3f(x, y, z)
