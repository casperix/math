package ru.casperix.math.color.rgb

import kotlinx.serialization.Serializable
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.misc.ColorDecoder
import ru.casperix.math.color.misc.ColorDecoder.floatToUByte
import ru.casperix.math.color.misc.ColorDecoder.getColorComponentToHex
import ru.casperix.math.color.rgba.RgbaColor4b
import ru.casperix.math.color.rgba.RgbaColor4d
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.vector.float32.Vector3f


@Serializable
data class RgbColor3f(val red: Float, val green: Float, val blue: Float) : RgbColor {
    constructor(component: Float) : this(component, component, component)

    override fun toColor3b() = RgbColor3b(floatToUByte(red), floatToUByte(green), floatToUByte(blue))
    override fun toColor3d() = RgbColor3d(red.toDouble(), green.toDouble(), blue.toDouble())
    override fun toColor3f() = this

    override fun toHSV(): HsvColor3f  {
        val R = red.coerceIn(0f..1f)
        val G = green.coerceIn(0f..1f)
        val B = blue.coerceIn(0f..1f)

        val max = maxOf(R, G, B)
        val min = minOf(R, G, B)

        val Hdegree = if (max == min) {
            0f// formally not defined
        } else if (max == R && G >= B) {
            60f * (G - B) / (max - min) + 0f
        } else if (max == R && G < B) {
            60f * (G - B) / (max - min) + 360f
        } else if (max == G) {
            60f * (B - R) / (max - min) + 120f
        } else if (max == B) {
            60f * (R - G) / (max - min) + 240f
        } else {
            throw Exception()
        }

        val H = Hdegree / 360f
        val V = max
        val S = if (max != 0f) {
            (max - min) / max
        } else 0f

        return HsvColor3f(H, S, V)
    }

}
