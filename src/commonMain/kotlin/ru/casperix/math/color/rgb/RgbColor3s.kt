package ru.casperix.math.color.rgb

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder

@Serializable
data class RgbColor3s(val red: UShort, val green: UShort, val blue: UShort) : RgbColor {
    constructor(component: UShort) : this(component, component, component)

    override fun toColor3b() = RgbColor3b(ColorDecoder.uShortToUByte(red), ColorDecoder.uShortToUByte(green), ColorDecoder.uShortToUByte(blue))
    override fun toColor3d() = RgbColor3d(ColorDecoder.uShortToDouble(red), ColorDecoder.uShortToDouble(green), ColorDecoder.uShortToDouble(blue))
    override fun toColor3f() = RgbColor3f(ColorDecoder.uShortToFloat(red), ColorDecoder.uShortToFloat(green), ColorDecoder.uShortToFloat(blue))
}