package ru.casperix.math.color.rgb

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.uByteToDouble
import ru.casperix.math.color.misc.ColorDecoder.uByteToFloat

@Serializable
data class RgbColor3b(val red: UByte, val green: UByte, val blue: UByte) : RgbColor {
    constructor(component: UByte) : this(component, component, component)

    override fun toColor3b() = this
    override fun toColor3d() = RgbColor3d(uByteToDouble(red), uByteToDouble(green), uByteToDouble(blue))
    override fun toColor3f() = RgbColor3f(uByteToFloat(red), uByteToFloat(green), uByteToFloat(blue))
}