package ru.casperix.math.color

import ru.casperix.math.color.hsv.HsvColor
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.color.rgba.RgbaColor4f

/**
 *  Color in the most general sense
 *
 *  To work specifically with concrete physical colors, see [ru.casperix.math.color.PhysicColor]
 */
interface Color {
    fun toRGB(): RgbColor3f
    fun toRGBA(): RgbaColor4f
    fun toHSV(): HsvColor3f
    fun toAlpha(): Float = toRGBA().toAlpha()

    companion object {
        fun rgb(red: Float, green: Float, blue: Float): RgbColor3f = RgbColor3f(red, green, blue)
        fun hsv(hue: Float, saturation: Float, value: Float): HsvColor3f = HsvColor3f(hue, saturation, value)
        fun rgba(red: Float, green: Float, blue: Float, alpha: Float): RgbaColor4f = RgbaColor4f(red, green, blue, alpha)

        val BLACK = RgbColor3f(0f, 0f, 0f)// 	Gray
        val SILVER = RgbColor3f(0.75f, 0.75f, 0.75f)// 	Gray
        val GRAY = RgbColor3f(0.5f, 0.5f, 0.5f)// 	Gray
        val WHITE = RgbColor3f(1f, 1f, 1f)// 	White
        val MAROON = RgbColor3f(0.5f, 0f, 0f)// 	Brown
        val RED = RgbColor3f(1f, 0f, 0f)// 	Red
        val PURPLE = RgbColor3f(0.5f, 0f, 0.5f)// 	Purple
        val FUCHSIA = RgbColor3f(1f, 0f, 1f)// 	Purple
        val GREEN = RgbColor3f(0f, 1f, 0f)// 	Green
        val LIME = RgbColor3f(0.25f, 1f, 0f)// 	Green
        val OLIVE = RgbColor3f(0.5f, 0.5f, 0f)// 	Green
        val YELLOW = RgbColor3f(1f, 1f, 0f)// 	Yellow
        val NAVY = RgbColor3f(0f, 0f, 0.5f)// 	Blue
        val BLUE = RgbColor3f(0f, 0f, 1f)// 	Blue
        val TEAL = RgbColor3f(0f, 0.5f, 0.5f)// 	Green
        val CYAN = RgbColor3f(0f, 1f, 1f)// 	Blue

        val all = listOf(BLACK, SILVER, GRAY, WHITE, MAROON, RED, PURPLE, FUCHSIA, GREEN, LIME, OLIVE, YELLOW, NAVY, BLUE, TEAL, CYAN)
    }
}