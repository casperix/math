package ru.casperix.math.color.rgba

import ru.casperix.math.color.Color
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.misc.ColorDecoder
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector4f

/**
 * Strictly speaking, it is the RGB-color AND alpha. Since this combination is often used as a "Color", this type is introduced.
 * To work specifically with colors, see [ru.casperix.math.color.PhysicColor]
 */
interface RgbaColor : Color {
    fun toColor4b(): RgbaColor4b
    fun toColor4d(): RgbaColor4d
    fun toColor4f(): RgbaColor4f

    fun toColor1i(): RgbaColor1i = toColor4b().run { RgbaColor1i(red, green, blue, alpha) }
    fun toVector4f(): Vector4f = toColor4f().run { Vector4f(red, green, blue, alpha) }

    fun toColor1f(): RgbaColor1f {
        val code = toColor1i()
        val int = code.value.toInt()
        val bits = Float.fromBits(int)
        return RgbaColor1f(bits)
    }

    override fun toHSV() = toRGB().toHSV()
    override fun toRGBA() = toColor4f()
    override fun toRGB() = toColor4f().run { RgbColor3f(red, green, blue) }
    override fun toAlpha() = toColor4f().run { alpha }

    fun toHexString(): String = toColor4f().run {
        ColorDecoder.getColorComponentToHex(red) +
                ColorDecoder.getColorComponentToHex(green) +
                ColorDecoder.getColorComponentToHex(blue) +
                ColorDecoder.getColorComponentToHex(alpha)
    }
}