package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder
import ru.casperix.math.color.misc.ColorDecoder.decodeComponentAsDouble
import ru.casperix.math.color.misc.ColorDecoder.decodeComponentAsFloat
import ru.casperix.math.color.misc.ColorDecoder.decodeComponentAsUByte

@Serializable
data class RgbaColor1i(val value: UInt) : RgbaColor {

    constructor(red: UByte, green: UByte, blue: UByte, alpha: UByte = UByte.MAX_VALUE)
            : this(ColorDecoder.uBytesToColor(red, green, blue, alpha))

    constructor(red: Int, green: Int, blue: Int, alpha: Int = 255)
            : this(ColorDecoder.intsToColor(red, green, blue, alpha))


    override fun toColor4b() = RgbaColor4b(
        decodeComponentAsUByte(0, value),
        decodeComponentAsUByte(1, value),
        decodeComponentAsUByte(2, value),
        decodeComponentAsUByte(3, value),
    )


    override fun toColor4d() = RgbaColor4d(
        decodeComponentAsDouble(0, value),
        decodeComponentAsDouble(1, value),
        decodeComponentAsDouble(2, value),
        decodeComponentAsDouble(3, value),
    )

    override fun toColor4f() = RgbaColor4f(
        decodeComponentAsFloat(0, value),
        decodeComponentAsFloat(1, value),
        decodeComponentAsFloat(2, value),
        decodeComponentAsFloat(3, value),
    )


    override fun toColor1i()=this
}

