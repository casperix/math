package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.uShortToDouble
import ru.casperix.math.color.misc.ColorDecoder.uShortToFloat
import ru.casperix.math.color.misc.ColorDecoder.uShortToUByte

/**
 * 16 bit per channel color.
 * every channel range 0-65535
 */
@Serializable
data class RgbaColor4s(val red: UShort, val green: UShort, val blue: UShort, val alpha: UShort) : RgbaColor {
    constructor(component: UShort) : this(component, component, component, component)

    override fun toColor4b() = RgbaColor4b(uShortToUByte(red), uShortToUByte(green), uShortToUByte(blue), uShortToUByte(alpha))
    override fun toColor4d() = RgbaColor4d(uShortToDouble(red), uShortToDouble(green), uShortToDouble(blue), uShortToDouble(alpha))
    override fun toColor4f() = RgbaColor4f(uShortToFloat(red), uShortToFloat(green), uShortToFloat(blue), uShortToFloat(alpha))
}