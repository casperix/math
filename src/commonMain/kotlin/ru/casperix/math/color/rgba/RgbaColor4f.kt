package ru.casperix.math.color.rgba

import kotlinx.serialization.Serializable
import ru.casperix.math.color.misc.ColorDecoder.floatToUByte

@Serializable
data class RgbaColor4f(val red: Float, val green: Float, val blue: Float, val alpha: Float) : RgbaColor {
    constructor(component: Float) : this(component, component, component, component)


    override fun toColor4b() = RgbaColor4b(floatToUByte(red), floatToUByte(green), floatToUByte(blue), floatToUByte(alpha))
    override fun toColor4d() = RgbaColor4d(red.toDouble(), green.toDouble(), blue.toDouble(), alpha.toDouble())
    override fun toColor4f() = this
}

