package ru.casperix.math.color.cmyk

import kotlinx.serialization.Serializable
import ru.casperix.math.color.hsv.HsvColor
import ru.casperix.math.color.hsv.HsvColor3f
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.vector.float32.Vector4f

@Deprecated(message = "Not yet implemented")
@Serializable
data class CmykColor4f(val cyan: Float, val magenta: Float, val yellow: Float, val key: Float) : CmykColor {
    constructor(component: Float) : this(component, component, component, component)

    override fun toRGB(): RgbColor3f {
        TODO("Not yet implemented")
    }

    override fun toRGBA(): RgbaColor4f {
        TODO("Not yet implemented")
    }

    override fun toHSV(): HsvColor3f {
        TODO("Not yet implemented")
    }
}
