package ru.casperix.math.color.misc

import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.color.rgba.RgbaColor1i
import ru.casperix.misc.*
import kotlin.math.roundToInt
import kotlin.text.toString

object ColorDecoder {

	fun uBytesToColor(red: UByte, green: UByte, blue: UByte, alpha: UByte): UInt {
		return intsToColor(red.toInt(), green.toInt(), blue.toInt(), alpha.toInt())
	}

	fun doubleToUByte(component: Double): UByte {
		return (component * 255.0).roundToInt().clamp(0, 256).toUByte()
	}

	fun floatToUByte(component: Float): UByte {
		return (component * 255f).roundToInt().clamp(0, 256).toUByte()
	}


	fun uByteToFloat(component: UByte): Float {
		return component.toFloat() / 255f
	}

	fun uByteToDouble(component: UByte): Double {
		return component.toDouble() / 255.0
	}
	fun uShortToDouble(component: UShort): Double {
		return component.toDouble() / 65535.0
	}
	fun uShortToFloat(component: UShort): Float {
		return component.toFloat() / 65535f
	}

	fun uShortToUByte(component: UShort): UByte {
		return (component / 255u).toUByte()
	}

	fun byteToFloat(component: Byte): Float {
		return component.toUByte().toFloat() / 255f
	}

	fun byteToDouble(component: Byte): Double {
		return component.toUByte().toDouble() / 255.0
	}

	fun bytesToColor(red: Byte, green: Byte, blue: Byte, alpha: Byte): UInt {
		var color = 0u
		color = encodeSignedByte(0, red, color)
		color = encodeSignedByte(1, green, color)
		color = encodeSignedByte(2, blue, color)
		color = encodeSignedByte(3, alpha, color)
		return color
	}

	fun intsToColor(red: Int, green: Int, blue: Int, alpha: Int): UInt {
		var color = 0u
		color = encodeUnsignedByte(0, red, color)
		color = encodeUnsignedByte(1, green, color)
		color = encodeUnsignedByte(2, blue, color)
		color = encodeUnsignedByte(3, alpha, color)
		return color.toUInt()
	}

	fun encodeUnsignedByte(index: Int, byte: Int, color: UInt): UInt {
		return color.toInt().setBits( index * 8, 8, byte.clamp(0, 256)).toUInt()
	}

	fun encodeSignedByte(index: Int, byte: Byte, color: UInt): UInt {
		return color.toInt().setBits(index * 8, 8, byte.toInt().clamp(0, 256)).toUInt()
	}

	fun decodeComponentAsUByte(index: Int, color: UInt): UByte {
		return color.toInt().getBits( index * 8, 8).toUByte()
	}

	fun decodeComponentAsByte(index: Int, color: UInt): Byte {
		return color.toInt().getBits( index * 8, 8).toByte()
	}

	fun decodeComponentAsDouble(index: Int, color: UInt): Double {
		return decodeComponentAsUByte(index, color).toDouble() / 255.0
	}

	fun decodeComponentAsFloat(index: Int, color: UInt): Float {
		return decodeComponentAsUByte(index, color).toFloat() / 255f
	}

	fun parseHex(source: String): RgbaColor1i {
		val r = source.substringOrEmpty(0, 2).toIntOrNull(16)?.toUByte() ?: 255u
		val g = source.substringOrEmpty(2, 4).toIntOrNull(16)?.toUByte() ?: 255u
		val b = source.substringOrEmpty(4, 6).toIntOrNull(16)?.toUByte() ?: 255u
		val a = source.substringOrEmpty(6, 8).toIntOrNull(16)?.toUByte() ?: 255u
		return RgbaColor1i(r, g, b, a)
	}

	fun getColorComponentToHex(value: Float): String {
		return getColorComponentToHex(value.toDouble())
	}

	fun getColorComponentToHex(value: Double): String {
		val max = 255.0
		val int = ((value * max).clamp(0.0, max)).roundToInt()
		return getColorComponentToHex(int)
	}

	fun getColorComponentToHex(value: UByte): String {
		return getColorComponentToHex(value.toInt())
	}

	fun getColorComponentToHex(value: Byte): String {
		return getColorComponentToHex(value.toInt())
	}

	fun getColorComponentToHex(value: Short): String {
		return getColorComponentToHex(value.toInt())
	}

	fun getColorComponentToHex(value: UShort): String {
		return getColorComponentToHex(value.toInt())
	}

	fun getColorComponentToHex(value: Int): String {
		val hex = value.coerceIn(0..255) .toString(16)
		return hex.fillSpace(2, '0')
	}

	fun RgbaColor.toHexRGB(): String = toColor4b().run {
		"#" +
				componentToHex(red) +
				componentToHex(green) +
				componentToHex(blue)
	}

	fun RgbaColor.toHexRGBA(): String = toColor4b().run {
		"#" +
				componentToHex(red) +
				componentToHex(green) +
				componentToHex(blue) +
				componentToHex(alpha)
	}

	fun componentToHex(component: UByte) = component.toHexString().uppercase().fillSpace(2, '0')

}