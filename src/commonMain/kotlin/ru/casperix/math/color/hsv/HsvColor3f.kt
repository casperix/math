package ru.casperix.math.color.hsv

import kotlinx.serialization.Serializable
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgb.toRGBColor
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.vector.float32.Vector3f
import kotlin.math.absoluteValue

/**
 * @param hue 0 -- 1
 * @param saturation 0 -- 1
 * @param value 0 -- 1
 */
@Serializable
data class HsvColor3f(val hue: Float, val saturation: Float, val value: Float) : HsvColor {
    constructor(component: Float) : this(component, component, component)

    override fun toVector3f() = Vector3f(hue, saturation, value)


    override fun toRGB(): RgbColor3f {
        val H = hue * 360f
        val Hi = (H.toInt() / 60).absoluteValue.mod(6)
        val V = value * 100f

        val Vmin = (1f - saturation) * value * 100f
        val a = (V - Vmin) * (H.mod(60f)) / 60f
        val Vinc = Vmin + a
        val Vdec = V - a

        val percents = when (Hi) {
            0 -> Vector3f(V, Vinc, Vmin)
            1 -> Vector3f(Vdec, V, Vmin)
            2 -> Vector3f(Vmin, V, Vinc)
            3 -> Vector3f(Vmin, Vdec, V)
            4 -> Vector3f(Vinc, Vmin, V)
            5 -> Vector3f(V, Vmin, Vdec)
            else -> Vector3f.ZERO
        }

        return (percents * 0.01f).toRGBColor()
    }

    override fun toRGBA() = toRGB().toRGBA()

    override fun toHSV() = this

}
