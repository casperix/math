package ru.casperix.math.angle

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.angle.float64.RadianDouble

fun radianOf(value: Float):RadianFloat {
    return RadianFloat(value)
}

fun radianOf(value: Double):RadianDouble {
    return RadianDouble(value)
}