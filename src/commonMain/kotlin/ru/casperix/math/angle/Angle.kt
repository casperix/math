package ru.casperix.math.angle

interface Angle<Self : Any, Num : Number> : Comparable<Self> {

    /**
     * @return angle in [0, 2π) interval
     */
    fun normalize(): Self

    fun isFinite(): Boolean

    operator fun plus(other: Self): Self

    operator fun minus(other: Self): Self

    operator fun plus(other: Num): Self
    operator fun minus(other: Num): Self

    operator fun times(factor: Num): Self
    operator fun div(factor: Num): Self

    operator fun unaryMinus(): Self

    operator fun compareTo(other: Num): Int

    override operator fun compareTo(other: Self): Int

    fun format(precision: Int = 2): String
}