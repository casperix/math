package ru.casperix.math.angle

import ru.casperix.math.angle.float64.DegreeDouble

fun degreeOf(value: Float): ru.casperix.math.angle.float32.DegreeFloat {
    return ru.casperix.math.angle.float32.DegreeFloat(value)
}

fun degreeOf(value: Double): DegreeDouble {
    return DegreeDouble(value)
}