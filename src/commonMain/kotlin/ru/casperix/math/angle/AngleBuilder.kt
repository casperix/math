package ru.casperix.math.angle

import ru.casperix.math.vector.api.AbstractVector2

interface AngleBuilder<Self : Any, Vector : AbstractVector2<Vector, Component>, Component : Number> {
    val ZERO: Self
    val MAX: Self

    fun byDirection(value: Vector): Self {
        return byDirection(value.x, value.y)
    }

    /**
     * @return angle from X-axis to vector in [-π, π) interval
     */
    fun byDirection(x: Component, y: Component): Self

}

