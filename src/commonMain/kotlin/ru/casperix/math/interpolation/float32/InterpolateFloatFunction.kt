package ru.casperix.math.interpolation.float32

import ru.casperix.math.geometry.fPI
import kotlin.math.*

typealias InterpolateTypeFunctionf<Type> = (Type, Type, Float) -> Type
typealias InterpolateFloatFunction = (Float, Float, Float) -> Float

/**
 * 	x= [0 ; 1]
 *
 * 	x = 0.0
 * 	value  grow from a to b
 * 	x = 0.5
 * 	value low from b to a
 * 	x = 1f
 */
val parabolaABAInterpolatef: InterpolateFloatFunction = { a: Float, b: Float, x: Float ->
	val v = if (x < 0.5) {
		x
	} else {
		1f - x
	}
	a + b * sqrt(v * 2f)
}

val linearInterpolatef: InterpolateFloatFunction = { a: Float, b: Float, x: Float ->
	a * (1f - x) + b * x
}


val cosineInterpolatef: InterpolateFloatFunction = { a: Float, b: Float, x: Float ->
	val ft = x * fPI
	val f = (1f - cos(ft)) * 0.5f
	a * (1f - f) + b * f
}


val hermiteInterpolatef: InterpolateFloatFunction = { a: Float, b: Float, x: Float ->
	a + (b - a) * (3f - 2f * x) * x * x
}

/**
 * @source https://spicyyoghurt.com/tools/easing-functions
 */

val easeInQuadf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	c * t * t + a
}

val easeOutQuadf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	-c * t * (t - 2) + a
}

val easeInOutQuadf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	val dt = t * 2f

	if (dt < 1f) {
		c / 2 * dt * dt + a
	} else {
		-c / 2 * ((dt - 1f) * (dt - 3f) - 1f) + a
	}
}

val easeInCubicf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	c * t * t * t + a
}

val easeOutCubicf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	val dt = t - 1f
	c * (dt * dt * dt + 1) + a
}

val easeInOutCubicf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val t1 = t * 2f
	val c = b - a

	if (t1 < 1f) {
		c / 2 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2f
		c / 2 * (t2 * t2 * t2 + 2) + a
	}
}

val easeInQuarticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	c * t * t * t * t + a
}

val easeOutQuarticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	val dt = t - 1f
	c * (dt * dt * dt * dt - 1) + a
}

val easeInOutQuarticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val t1 = t * 2f
	val c = b - a

	if (t1 < 1f) {
		c / 2 * t1 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2f
		c / 2 * (t2 * t2 * t2 * t2 - 2) + a
	}
}

val easeInQuinticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	c * t * t * t * t * t + a
}

val easeOutQuinticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val c = b - a
	val dt = t - 1f
	c * (dt * dt * dt * dt * dt + 1) + a
}

val easeInOutQuinticf: InterpolateFloatFunction = { a: Float, b: Float, t: Float ->
	val t1 = t * 2f
	val c = b - a

	if (t1 < 1f) {
		c / 2 * t1 * t1 * t1 * t1 * t1 + a
	} else {
		val t2 = t1 - 2f
		c / 2 * (t2 * t2 * t2 * t2 * t2 + 2) + a
	}
}

val easeInOutElasticf: InterpolateFloatFunction = { b: Float, a1: Float, t: Float ->
	val c = a1 - b

	val s: Float
	val p = 0.3f * 1.5f
	var a = c
	if (t == 0f) {
		b
	} else {
		val t1 = t * 2f
		if (t1 == 2f) {
			b + c
		} else {

			if (a < c.absoluteValue) {
				a = c
				s = p / 4
			} else {
				s = p / (2 * fPI) * asin(c / a)
			}
			val t2 = t1 - 1f
			if (t1 < 1) {
				-.5f * (a * 2f.pow(10 * t2) * sin((t2 - s) * (2 * fPI) / p)) + b
			} else {
				a * 2f.pow(-10 * t2) * sin((t2 - s) * (2 * fPI) / p) * .5f + c + b;
			}
		}
	}

}