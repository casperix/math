package ru.casperix.math.interpolation

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.angle.float64.DegreeDouble
import ru.casperix.math.angle.float64.RadianDouble
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.interpolation.float32.InterpolateFloatFunction
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.interpolation.float32.linearInterpolatef
import ru.casperix.math.interpolation.float64.InterpolateDoubleFunction
import ru.casperix.math.interpolation.float64.InterpolationDouble
import ru.casperix.math.interpolation.float64.linearInterpolate
import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float32.Vector4f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.float64.Vector4d


fun interpolateOf(
    startAngle: Color,
    finishAngle: Color,
    position: Float,
    interpolator: InterpolateFloatFunction = linearInterpolatef
): Color {
    return InterpolationFloat.color(startAngle, finishAngle, position, interpolator)
}

fun interpolateOf(
    startAngle: RgbaColor,
    finishAngle: RgbaColor,
    position: Float,
    interpolator: InterpolateFloatFunction = linearInterpolatef
): RgbaColor {
    return InterpolationFloat.rgba(startAngle, finishAngle, position, interpolator)
}

fun interpolateOf(
    startAngle: RgbColor,
    finishAngle: RgbColor,
    position: Float,
    interpolator: InterpolateFloatFunction = linearInterpolatef
): RgbColor {
    return InterpolationFloat.rgb(startAngle, finishAngle, position, interpolator)
}

fun interpolateOf(
    startAngle: DegreeFloat,
    finishAngle: DegreeFloat,
    position: Float,
    interpolator: InterpolateFloatFunction = linearInterpolatef
): DegreeFloat {
    return InterpolationFloat.angle(startAngle.toRadian(), finishAngle.toRadian(), position, interpolator).toDegree()
}

fun interpolateOf(
    startAngle: DegreeDouble,
    finishAngle: DegreeDouble,
    position: Double,
    interpolator: InterpolateDoubleFunction = linearInterpolate
): DegreeDouble {
    return InterpolationDouble.angle(startAngle.toRadian(), finishAngle.toRadian(), position, interpolator).toDegree()
}


fun interpolateOf(
    startAngle: RadianFloat,
    finishAngle: RadianFloat,
    position: Float,
    interpolator: InterpolateFloatFunction = linearInterpolatef
): RadianFloat {
    return InterpolationFloat.angle(startAngle, finishAngle, position, interpolator)
}

fun interpolateOf(
    startAngle: RadianDouble,
    finishAngle: RadianDouble,
    position: Double,
    interpolator: InterpolateDoubleFunction = linearInterpolate
): RadianDouble {
    return InterpolationDouble.angle(startAngle, finishAngle, position, interpolator)
}


fun interpolateOf(first: Float, second: Float, t: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): Float {
    return InterpolationFloat.single(first, second, t, interpolator)
}

fun interpolateOf(first: Double, second: Double, t: Double, interpolator: InterpolateDoubleFunction = linearInterpolate): Double {
    return InterpolationDouble.single(first, second, t, interpolator)
}

fun interpolateOf(first: Vector2f, second: Vector2f, t: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): Vector2f {
    return InterpolationFloat.vector2(first, second, t, interpolator)
}

fun interpolateOf(first: Vector2d, second: Vector2d, t: Double, interpolator: InterpolateDoubleFunction = linearInterpolate): Vector2d {
    return InterpolationDouble.vector2(first, second, t, interpolator)
}

fun interpolateOf(first: Vector3f, second: Vector3f, t: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): Vector3f {
    return InterpolationFloat.vector3(first, second, t, interpolator)
}

fun interpolateOf(first: Vector3d, second: Vector3d, t: Double, interpolator: InterpolateDoubleFunction = linearInterpolate): Vector3d {
    return InterpolationDouble.vector3(first, second, t, interpolator)
}

fun interpolateOf(first: Vector4f, second: Vector4f, t: Float, interpolator: InterpolateFloatFunction = linearInterpolatef): Vector4f {
    return InterpolationFloat.vector4(first, second, t, interpolator)
}

fun interpolateOf(first: Vector4d, second: Vector4d, t: Double, interpolator: InterpolateDoubleFunction = linearInterpolate): Vector4d {
    return InterpolationDouble.vector4(first, second, t, interpolator)
}

fun interpolateOf(q1: QuaternionFloat, q2: QuaternionFloat, factor: Float): QuaternionFloat {
    return InterpolationFloat.quaternion(q1, q2, factor)
}

fun interpolateOf(q1: QuaternionDouble, q2: QuaternionDouble, factor: Double): QuaternionDouble {
    return InterpolationDouble.quaternion(q1, q2, factor)
}
