package ru.casperix.math.straight_line

import ru.casperix.math.vector.api.AbstractVector

interface StraightLine<Point : AbstractVector<Point>, Item : Number> : AbstractFunction<Item> {
    val b: Item
    val m: Item
}


