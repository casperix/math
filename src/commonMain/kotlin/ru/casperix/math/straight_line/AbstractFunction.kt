package ru.casperix.math.straight_line

fun interface AbstractFunction<Item:Number> {
    fun get(x: Item): Item
}