package ru.casperix.math.straight_line

import ru.casperix.math.vector.api.AbstractVector

interface LineSegment<Point : AbstractVector<Point>> {
    val start: Point
    val finish: Point
}