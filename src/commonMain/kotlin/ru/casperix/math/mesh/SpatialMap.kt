package ru.casperix.math.mesh

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f


interface SpatialMap<Element : Any> {
    fun clear()

    fun all(): List<Element>

    fun add(element: Element)
    fun remove(element: Element)

    fun has(element: Element): Boolean

    fun search(area: Box2f): Collection<Element>

    fun search(area: Circle2f): Collection<Element>

    fun search(area: Polygon2f): Collection<Element>
    fun search(area: LineSegment2f): Collection<Element>


    fun search(center: Vector2f, range: Float): Collection<Element> {
        return search(Circle2f(center, range))
    }

    fun addAll(elements: Collection<Element>) {
        elements.forEach {
            add(it)
        }
    }

    fun removeAll(elements: Collection<Element>) {
        elements.forEach {
            remove(it)
        }
    }

}