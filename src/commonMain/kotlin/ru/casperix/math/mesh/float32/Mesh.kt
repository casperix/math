package ru.casperix.math.mesh.float32

data class Mesh(
    val points: List<MeshPoint> = emptyList(),
    val edges: List<MeshEdge> = emptyList(),
    val regions: List<MeshRegion> = emptyList(),
)

