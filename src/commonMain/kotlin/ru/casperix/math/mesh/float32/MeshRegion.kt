package ru.casperix.math.mesh.float32

import ru.casperix.math.geometry.Polygon2f

data class MeshRegion(val shape: Polygon2f, val tag: MeshRegionTag? = null)