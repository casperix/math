package ru.casperix.math.mesh.float32

import ru.casperix.math.vector.float32.Vector2f

data class MeshPoint(val shape: Vector2f, val tag: PointTag? = null)