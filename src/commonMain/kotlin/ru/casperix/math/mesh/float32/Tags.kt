package ru.casperix.math.mesh.float32

import ru.casperix.math.color.rgba.RgbaColor

interface PointTag

interface MeshEdgeTag

interface MeshRegionTag

data class ColorTag(val color: RgbaColor) : PointTag, MeshEdgeTag, MeshRegionTag