package ru.casperix.math.mesh

import ru.casperix.math.mesh.float32.*
import ru.casperix.math.geometry.float32.Geometry2Float
import ru.casperix.math.vector.float32.Vector2f


interface MeshBuilder {
    val points: SpatialMap<MeshPoint>
    val edges: SpatialMap<MeshEdge>
    val regions: SpatialMap<MeshRegion>

    fun add(point: MeshPoint) {
        points.add(point)
    }

    fun add(edge: MeshEdge) {
        edges.add(edge)
    }

    fun add(region: MeshRegion) {
        regions.add(region)
    }

    fun build(): Mesh {
        return Mesh(points.all(), edges.all(), regions.all())
    }

    fun nearPointOrNull(center: Vector2f, range: Float): MeshPoint? {
        return points.search(center, range).minByOrNull { it.shape.distTo(center) }
    }

    fun nearEdgeOrNull(center: Vector2f, range: Float): MeshEdge? {
        return edges.search(center, range).minByOrNull { Geometry2Float.distPointToSegment(center, it.shape.toLine()) }
    }

    fun nearPointInSegmentOrNull(center: Vector2f, range: Float): Vector2f? {
        val edgeCandidates = edges.search(center, range)
        val nearEdge = edgeCandidates.map { edge ->
            Pair(Geometry2Float.distPointToSegment(center, edge.shape.toLine()), edge)
        }.filter { (dist, _) -> dist <= range }.minByOrNull { (dist, _) -> dist }?.second ?: return null

        return listOf(nearEdge.shape.start, nearEdge.shape.finish).minBy {
            it.distTo(center)
        }
    }
}