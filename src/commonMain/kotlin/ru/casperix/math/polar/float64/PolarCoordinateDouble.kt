package ru.casperix.math.polar.float64

import ru.casperix.math.angle.float64.RadianDouble
import ru.casperix.math.geometry.DEGREE_TO_RADIAN
import ru.casperix.math.vector.float64.Vector2d
import kotlinx.serialization.Serializable
import kotlin.math.cos
import kotlin.math.sin

@Serializable
data class PolarCoordinateDouble(val range: Double, val angle: RadianDouble) {
//    constructor(range: Double, angle: DegreeDouble) : this(range, angle.toRadian())

    fun toDecart(): Vector2d {
        return Vector2d(range * cos(angle.value), range * sin(angle.value))
    }

    companion object {
        fun byRadian(range: Double, radian: Double): PolarCoordinateDouble {
            return PolarCoordinateDouble(range, RadianDouble(radian))
        }

        fun byDegree(range: Double, degree: Double): PolarCoordinateDouble {
            return PolarCoordinateDouble(range, RadianDouble(degree * DEGREE_TO_RADIAN))
        }
    }
}



