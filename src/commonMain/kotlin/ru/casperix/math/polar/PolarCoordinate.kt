package ru.casperix.math.polar

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.angle.float64.DegreeDouble
import ru.casperix.math.angle.float64.RadianDouble
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.polar.float64.PolarCoordinateDouble
import ru.casperix.math.quaternion.float32.QuaternionFloat
import ru.casperix.math.quaternion.float64.QuaternionDouble

fun polarCoordinateOf(range: Float, angle: RadianFloat): PolarCoordinateFloat {
    return PolarCoordinateFloat(range, angle)
}

fun polarCoordinateOf(range: Double, angle: RadianDouble): PolarCoordinateDouble {
    return PolarCoordinateDouble(range, angle)
}

fun polarCoordinateOf(range: Float, angle: ru.casperix.math.angle.float32.DegreeFloat): PolarCoordinateFloat {
    return PolarCoordinateFloat(range, angle.toRadian())
}

fun polarCoordinateOf(range: Double, angle: DegreeDouble): PolarCoordinateDouble {
    return PolarCoordinateDouble(range, angle.toRadian())
}
