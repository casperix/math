package ru.casperix.math.intersection

import ru.casperix.math.curve.CurvePositionEntry

class CurveIntersection(val first: CurvePositionEntry, val second: CurvePositionEntry) {
    val position = first.position

    fun reversed():CurveIntersection {
        return CurveIntersection(second, first)
    }
}