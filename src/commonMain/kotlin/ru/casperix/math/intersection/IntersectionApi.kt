package ru.casperix.math.intersection

import ru.casperix.math.geometry.Line
import ru.casperix.math.geometry.Polygon
import ru.casperix.math.geometry.Quad
import ru.casperix.math.geometry.Triangle
import ru.casperix.math.vector.api.AbstractVector

/**
 * 	https://blackpawn.com/texts/pointinpoly/default.html
 */
interface IntersectionApi<Point : AbstractVector<Point>> {
    fun hasPointWithLine(P: Point, T: Line<Point>): Boolean

    fun hasPointWithSegment(P: Point, T: Line<Point>): Boolean

    fun hasPointWithTriangle(P: Point, triangle: Triangle<Point>): Boolean

    fun hasPointWithPolygon(P: Point, polygon: Polygon<Point>): Boolean

    fun hasTriangleWithTriangle(a: Triangle<Point>, b: Triangle<Point>): Boolean

    fun hasQuadWithTriangle(a: Quad<Point>, b: Triangle<Point>): Boolean

    fun getLineWithLine(S: Line<Point>, T: Line<Point>): Point?

    fun getSegmentWithSegment(S: Line<Point>, T: Line<Point>): Point?

    fun hasQuadWithQuad(a: Quad<Point>, b: Quad<Point>): Boolean

    fun hasPointWithLine(P: Point, start: Point, finish: Point): Boolean {
        return hasPointWithLine(P, Line(start, finish))
    }

    fun hasLineWithPoint(start: Point, finish: Point, P: Point): Boolean {
        return hasPointWithLine(P, Line(start, finish))
    }

    fun hasSegmentWithTriangle(S: Line<Point>, T: Triangle<Point>): Boolean {
        if (getSegmentWithSegment(S, T.getEdge(0)) != null) return true
        if (getSegmentWithSegment(S, T.getEdge(1)) != null) return true
        if (getSegmentWithSegment(S, T.getEdge(2)) != null) return true
        if (hasPointWithTriangle(S.v0, T)) return true
        if (hasPointWithTriangle(S.v1, T)) return true
        return false
    }

    fun hasTriangleWithSegment(T: Triangle<Point>, S: Line<Point>): Boolean {
        return hasSegmentWithTriangle(S, T)
    }

    fun hasPointWithQuad(point: Point, value: Quad<Point>): Boolean {
        return hasPointWithTriangle(point, value.getFace(0)) || hasPointWithTriangle(point, value.getFace(1))
    }

    fun hasTriangleWithQuad(a: Triangle<Point>, b: Quad<Point>): Boolean {
        return hasQuadWithTriangle(b, a)
    }

    fun hasPointWithTriangle(P: Point, A: Point, B: Point, C: Point): Boolean {
        return hasPointWithTriangle(P, Triangle(A, B, C))
    }

    fun hasPointWithQuad(P: Point, A: Point, B: Point, C: Point, D: Point): Boolean {
        return hasPointWithQuad(P, Quad(A, B, C, D))
    }

    fun getSegmentWithSegment(start1: Point, finish1: Point, start2: Point, finish2: Point): Point? {
        return getSegmentWithSegment(Line(start1, finish1), Line(start2, finish2))
    }

    fun hasQuadWithPoint(value: Quad<Point>, point: Point): Boolean {
        return hasPointWithQuad(point, value)
    }

    fun hasTriangleWithPoint(A: Point, B: Point, C: Point, P: Point): Boolean {
        return hasPointWithTriangle(P, Triangle(A, B, C))
    }

    fun hasQuadWithPoint(A: Point, B: Point, C: Point, D: Point, P: Point): Boolean {
        return hasPointWithQuad(P, Quad(A, B, C, D))
    }

    fun hasLineWithPoint(T: Line<Point>, P: Point): Boolean {
        return hasPointWithLine(P, T)
    }

    fun hasSegmentWithPoint(T: Line<Point>, P: Point): Boolean {
        return hasPointWithSegment(P, T)
    }

    fun hasTriangleWithPoint(triangle: Triangle<Point>, P: Point): Boolean {
        return hasPointWithTriangle(P, triangle)
    }

    fun hasPolygonWithPoint(polygon: Polygon<Point>, P: Point): Boolean {
        return hasPointWithPolygon(P, polygon)
    }

}
