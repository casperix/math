package ru.casperix.math.bytes

class ByteArrayWriterImpl(val bytes: ByteArray, override val order: ByteOrder) : ByteArrayWriter {

    override fun writeByte(offset: Int, value: Byte) {
        bytes[offset] = value
    }

    override fun writeShort(offset: Int, value: Short) {
        if (order == ByteOrder.BIG_ENDIAN) {
            bytes[offset + 0] = (value.toInt() shr 8).toByte()
            bytes[offset + 1] = (value.toInt()).toByte()
        } else {
            bytes[offset + 0] = (value.toInt()).toByte()
            bytes[offset + 1] = (value.toInt() shr 8).toByte()
        }
    }

    override fun writeInt(offset: Int, value: Int) {
        if (order == ByteOrder.BIG_ENDIAN) {
            bytes[offset + 0] = (value shr 24).toByte()
            bytes[offset + 1] = (value shr 16).toByte()
            bytes[offset + 2] = (value shr 8).toByte()
            bytes[offset + 3] = (value).toByte()
        } else {
            bytes[offset + 0] = (value).toByte()
            bytes[offset + 1] = (value shr 8).toByte()
            bytes[offset + 2] = (value shr 16).toByte()
            bytes[offset + 3] = (value shr 24).toByte()
        }
    }

    override fun writeLong(offset: Int, value: Long) {
        if (order == ByteOrder.BIG_ENDIAN) {
            bytes[offset + 0] = (value shr 56).toByte()
            bytes[offset + 1] = (value shr 48).toByte()
            bytes[offset + 2] = (value shr 40).toByte()
            bytes[offset + 3] = (value shr 32).toByte()
            bytes[offset + 4] = (value shr 24).toByte()
            bytes[offset + 5] = (value shr 16).toByte()
            bytes[offset + 6] = (value shr 8).toByte()
            bytes[offset + 7] = (value).toByte()
        } else {
            bytes[offset + 0] = (value).toByte()
            bytes[offset + 1] = (value shr 8).toByte()
            bytes[offset + 2] = (value shr 16).toByte()
            bytes[offset + 3] = (value shr 24).toByte()
            bytes[offset + 4] = (value shr 32).toByte()
            bytes[offset + 5] = (value shr 40).toByte()
            bytes[offset + 6] = (value shr 48).toByte()
            bytes[offset + 7] = (value shr 56).toByte()
        }
    }

    override fun writeFloat(offset: Int, value: Float) {
        writeInt(offset, value.toBits())
    }

    override fun writeDouble(offset: Int, value: Double) {
        writeLong(offset, value.toBits())
    }
}

