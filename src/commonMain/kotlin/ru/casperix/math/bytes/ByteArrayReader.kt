package ru.casperix.math.bytes

interface ByteArrayReader {
    val order:ByteOrder
    fun readByte(offset:Int):Byte
    fun readShort(offset:Int):Short
    fun readInt(offset:Int):Int
    fun readFloat(offset:Int):Float
    fun readDouble(offset:Int):Double
    fun readLong(offset:Int):Long
}