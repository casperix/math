package ru.casperix.math.bytes

interface ByteArrayWriter {
    val order: ByteOrder
    fun writeByte(offset: Int, value: Byte)
    fun writeShort(offset: Int, value: Short)
    fun writeInt(offset: Int, value: Int)
    fun writeFloat(offset: Int, value: Float)
    fun writeDouble(offset: Int, value: Double)
    fun writeLong(offset: Int, value: Long)
}