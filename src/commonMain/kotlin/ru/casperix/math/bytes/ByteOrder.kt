package ru.casperix.math.bytes

enum class ByteOrder {
    BIG_ENDIAN,
    LITTLE_ENDIAN,
}