package ru.casperix.math.geometry

import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector3i


/**
 *
 *        3/----/7
 * 	  1/----/5
 *
 * 		  2/----/6
 * 		0/----/4
 * Z
 * |
 * |   Y
 * /===X
 *
 *
 * 	Естественный порядок следования вершин:
 * 	Получим его если мы будем итерировать все координаты по очереди
 *
 * 	for (x in1..2)
 * 		for (y in 1..2)
 * 			for (z in 1..2)
 * 				...
 *
 */
data class Octagon<Point:Any>(val v0: Point, val v1: Point, val v2: Point, val v3: Point, val v4: Point, val v5: Point, val v6: Point, val v7: Point) : Polygon<Point> {

	override fun <TargetPoint:Any> convert(converter: (Point) -> TargetPoint): Octagon<TargetPoint> {
		return Octagon(
				converter(v0),
				converter(v1),
				converter(v2),
				converter(v3),
				converter(v4),
				converter(v5),
				converter(v6),
				converter(v7)
		)
	}

	override fun getVertexAmount(): Int {
		return 8
	}

	override fun getEdge(index: Int): Line<Point> {
		TODO("Not yet implemented")
	}

	override fun getEdgeAmount(): Int {
		TODO("Not yet implemented")
	}


	override fun getEdgeList(): List<Line<Point>> {
		return (0 until getEdgeAmount()).map {
			getEdge(it)
		}
	}

	fun getFaces(): List<Quad<Point>> {
		return listOf(getFace(0), getFace(1), getFace(2), getFace(3), getFace(4), getFace(5))
	}

	fun getFaceAmount(): Int {
		return 6
	}

	override fun getVertex(index: Int): Point {
		return when (index) {
			0 -> v0
			1 -> v1
			2 -> v2
			3 -> v3
			4 -> v4
			5 -> v5
			6 -> v6
			7 -> v7
			else -> throw Error("Invalid index")
		}
	}

	fun getFace(index: Int): Quad<Point> {
		return when (index) {
			0 -> Quad(v0, v2, v3, v1)    //	-x
			1 -> Quad(v4, v5, v7, v6)    //	+x
			2 -> Quad(v0, v1, v5, v4)    //	-y
			3 -> Quad(v2, v6, v7, v3)    //	+y
			4 -> Quad(v0, v4, v6, v2)    //	-z
			5 -> Quad(v1, v3, v7, v5)    //	+z
			else -> throw Error("Invalid index")
		}
	}
}

typealias Octagon3d = Octagon<Vector3d>
typealias Octagon3i = Octagon<Vector3i>

fun Octagon3d(start: Vector3d, dimension: Vector3d): Octagon3d = Octagon3d(start, start + dimension * Vector3d.Z, start + dimension * Vector3d.Y, start + dimension * Vector3d.YZ, start + dimension * Vector3d.X, start + dimension * Vector3d.XZ, start + dimension * Vector3d.XY, start + dimension * Vector3d.ONE)

fun Octagon3i(start: Vector3i, dimension: Vector3i): Octagon3i = Octagon3i(start, start + dimension * Vector3i.Z, start + dimension * Vector3i.Y, start + dimension * Vector3i.YZ, start + dimension * Vector3i.X, start + dimension * Vector3i.XZ, start + dimension * Vector3i.XY, start + dimension * Vector3i.ONE)

fun Octagon3d(start: Vector3d, xAxis: Vector3d, yAxis: Vector3d, zAxis: Vector3d): Octagon3d {
	return Octagon3d(start, start + zAxis, start + yAxis, start + yAxis + zAxis, start + xAxis, start + zAxis + xAxis, start + xAxis + yAxis, start + xAxis + yAxis + zAxis)
}


fun octagonByCenter(center: Vector3d, xAxis: Vector3d, yAxis: Vector3d, zAxis: Vector3d): Octagon3d {
	return Octagon3d(center - (xAxis + yAxis + zAxis) * 0.5, xAxis, yAxis, zAxis)
}