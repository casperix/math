package ru.casperix.math.geometry.builder

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.polar.float32.PolarCoordinateFloat

internal class PointCache(steps: Int) {
    val points = (0..steps).map { step ->
        val angle = RadianFloat.PI2 * (step / steps.toFloat())
        PolarCoordinateFloat(1f, angle).toDecart()
    }

    companion object {
        val pointCache = mutableMapOf<Int, PointCache>()
    }

}