package ru.casperix.math.geometry.builder

sealed interface ArrowMode

class FixedSizeArrowMode(val length:Float = 1f, val maxThick:Float = 0.5f) : ArrowMode

class ProportionalArrowMode(val lengthFactor:Float = 0.25f, val thickFactor: Float = 2f) : ArrowMode


class UniformArrowMode(val maxLengthFactor:Float = 0.5f, val length:Float = 1f, val maxThick:Float = 0.5f) : ArrowMode
