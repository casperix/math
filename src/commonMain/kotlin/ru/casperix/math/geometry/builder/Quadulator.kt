package ru.casperix.math.geometry.builder

import ru.casperix.math.curve.float32.Curve2f
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.vector.float32.Vector2f

object Quadulator {
    fun circle(center: Vector2f, rangeInside: Float, rangeOutside: Float, steps: Int = 32): List<Quad2f> {
        val points = PointCache.pointCache.getOrPut(steps) { PointCache(steps) }.points
        return (0 until points.size - 1).map { index ->
            val p00 = points[index] * rangeInside + center
            val p10 = points[index + 1] * rangeInside + center
            val p11 = points[index + 1] * rangeOutside + center
            val p01 = points[index] * rangeOutside + center

            Quad2f(p00, p01, p11, p01)
        }
    }

    fun curve(curve: Curve2f, width: Float, parts: Int = 100): List<Quad2f> {
        val range = width / 2f
        val pointPairs = (0..parts).map {
            val t = it.toFloat() / parts
            val pivot = curve.getPosition(t)
            val left = curve.getNormal(t) * range
            Pair(pivot - left, pivot + left)
        }

        return curve(pointPairs)
    }

    private fun curve(points: List<Pair<Vector2f, Vector2f>>): List<Quad2f> {
        return (0 until points.size - 1).map { partId ->
            val (A, D) = points[partId]
            val (B, C) = points[partId + 1]
            Quad2f(A, B, C, D)
        }
    }
}