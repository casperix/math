package ru.casperix.math.geometry

enum class PointAroundRay {
    LEFT,
    INSIDE,
    RIGHT
}

