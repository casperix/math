package ru.casperix.math.geometry

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.VectorArithmetic
import ru.casperix.math.vector.api.AbstractVectorN

fun <CustomPoint : VectorArithmetic<CustomPoint, Item>, Item : Number> Line<CustomPoint>.length(): Item {
    return v1.distTo(v0)
}

fun <CustomPoint : VectorArithmetic<CustomPoint, Item>, Item : Number> Line<CustomPoint>.delta(): CustomPoint {
    return v1 - v0
}

fun <CustomPoint: AbstractVectorN<CustomPoint, Item>, Item : Number> Line<CustomPoint>.tangent(): CustomPoint {
    return delta().normalize()
}



fun <CustomPoint: AbstractVectorN<CustomPoint, Item>, Item : Number> Line<CustomPoint>.scale(scale:Item): Line<CustomPoint> {
    val center = (v0 + v1).half()
    return Line(center + (v0 - center) * scale, center + (v1 - center) * scale)
}





fun Line2f.toSegment():LineSegment2f {
    return LineSegment2f(v0, v1)
}