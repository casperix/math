package ru.casperix.math.geometry.float64

import ru.casperix.math.geometry.Quad2d
import ru.casperix.math.geometry.Quad2i
import ru.casperix.math.geometry.Quad3d
import ru.casperix.math.vector.float64.Vector2d


fun Quad2d.addDimension(value: Double): Quad3d {
    return convert { it.addDimension(value) }
}
fun Quad2d.toQuad2i(): Quad2i {
    return convert { it.toVector2i() }
}

fun Quad2d.rotate(angle: Double): Quad2d {
    return convert {
        val p = it.toPolar()
        p.copy(angle = p.angle + angle).toDecart()
    }
}

fun Quad2d.translate(offset: Vector2d): Quad2d {
    return convert { it + offset }
}

