package ru.casperix.math.geometry.float64

import ru.casperix.math.vector.float64.Vector3d

data class Plane3d(val normal: Vector3d, val destToOrigin:Double)