package ru.casperix.math.geometry.float64

import ru.casperix.math.axis_aligned.float64.Box3d
import ru.casperix.math.geometry.Line3d
import ru.casperix.math.vector.float64.Vector3d

class Capsule3d(val path: Line3d, val range: Double) {
	fun getBox(pathFactor: Double): Box3d {
		val pathPosition = path.v0 * (1.0 - pathFactor) + path.v1 * pathFactor
		return Box3d(pathPosition - Vector3d(range), pathPosition + Vector3d(range))
	}
}