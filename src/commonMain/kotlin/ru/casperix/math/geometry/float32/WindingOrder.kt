package ru.casperix.math.geometry.float32

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.collection.getLooped
import ru.casperix.math.geometry.PointAroundRay
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.geometry.RotateDirection
import ru.casperix.math.vector.float32.Vector2f
import kotlin.math.PI

fun Polygon2f.getWindingOrder(): RotateDirection {
    val vertices = getVertices()

    var accumulatedAngle = RadianFloat.ZERO
    vertices.indices.forEach {
        accumulatedAngle += getWindingAngle(vertices, it)
    }

    return getWindingOrder(accumulatedAngle)
}

fun getWindingAngle(vertices: List<Vector2f>, vertexIndex: Int): RadianFloat {
    val last = vertices.getLooped(vertexIndex - 1)
    val current = vertices.getLooped(vertexIndex)
    val next = vertices.getLooped(vertexIndex + 1)
    val directedAngle = RadianFloat.betweenDirectionsDirected(current - last, current-next)
    return directedAngle - RadianFloat.PI
}

fun getWindingOrder(angle: RadianFloat): RotateDirection {
    return if (angle >= RadianFloat.ZERO) {
        RotateDirection.COUNTERCLOCKWISE
    } else {
        RotateDirection.CLOCKWISE
    }
}

fun getWindingOrder(vertices: List<Vector2f>, vertexIndex: Int): RotateDirection {
//  TODO: need check commented
//    val last = vertices.getLooped(vertexIndex - 1)
//    val current = vertices.getLooped(vertexIndex)
//    val next = vertices.getLooped(vertexIndex + 1)
//    val par = Geometry2Float.getPointAroundRay(last, current, next, 0f)
//    return if (par == PointAroundRay.LEFT) {
//        RotateDirection.COUNTERCLOCKWISE
//    } else {
//        RotateDirection.CLOCKWISE
//    }
    return getWindingOrder(getWindingAngle(vertices, vertexIndex))
}
