package ru.casperix.math.geometry.float32

import ru.casperix.math.curve.float32.LineCurve2f
import ru.casperix.math.geometry.*
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.rotateCCW

fun Line2f.addDimension(z0: Float, z1: Float): Line3f {
    return Line3f(v0.addDimension(z0), v1.addDimension(z1))
}


fun Line2f.normal(): Vector2f {
    return tangent().run { rotateCCW() }
}


fun Line2f.toLine2d(): Line2d {
    return Line2d(v0.toVector2d(), v1.toVector2d())
}

fun Line2f.median():Vector2f {
    return (v0 + v1) / 2f
}

fun Line2f.invert(): Line2f {
    return Line2f(v1, v0)
}

fun LineSegment2f.scale(factor: Float): LineSegment2f {
    return LineSegment2f(start * factor, finish * factor)
}


fun LineCurve2f.line(): Line2f {
    return Line2f(start, finish)
}

fun Line2f.direction(): Vector2f {
    return delta().normalize()
}

fun LineSegment2f.translate(offset: Vector2f): LineSegment2f {
    return LineSegment2f(start + offset, finish + offset)
}