package ru.casperix.math.geometry.float32

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.geometry.Quad2d
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.Quad3f
import ru.casperix.math.vector.float32.Vector2f

fun Quad2f.addDimension(value: Float): Quad3f {
    return convert { it.addDimension(value) }
}


fun Quad2f.toQuad2d(): Quad2d {
    return convert { it.toVector2d() }
}


@Deprecated(message = "use RadianFloat")
fun Quad2f.rotate(angle: Float): Quad2f {
    return convert {
        val p = it.toPolar()
        p.copy(angle = p.angle + angle).toDecart()
    }
}


fun Quad2f.median(): Vector2f {
    return (v0 + v1 + v2 + v3) / 4f
}


fun Quad2f.rotate(angle: RadianFloat): Quad2f {
    return convert {
        it.rotate(angle)
    }
}

fun Quad2f.rotate(angle: ru.casperix.math.angle.float32.DegreeFloat): Quad2f {
    return convert {
        it.rotate(angle)
    }
}

fun Quad2f.translate(offset: Vector2f): Quad2f {
    return convert { it + offset }
}
