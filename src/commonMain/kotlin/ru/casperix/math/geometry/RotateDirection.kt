package ru.casperix.math.geometry

enum class RotateDirection {
    CLOCKWISE,
    COUNTERCLOCKWISE
}