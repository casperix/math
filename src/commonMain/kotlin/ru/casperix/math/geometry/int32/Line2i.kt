package ru.casperix.math.geometry.int32

import ru.casperix.math.geometry.Line2i
import ru.casperix.math.geometry.Line3i

fun Line2i.addDimension(z0: Int, z1: Int): Line3i {
    return Line3i(v0.addDimension(z0), v1.addDimension(z1))
}
