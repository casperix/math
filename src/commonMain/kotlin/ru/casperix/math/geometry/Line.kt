package ru.casperix.math.geometry

import ru.casperix.math.vector.api.AbstractVector
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i
import kotlinx.serialization.Serializable


typealias Line3d = Line<Vector3d>
typealias Line3f = Line<Vector3f>
typealias Line3i = Line<Vector3i>

@Deprecated(message = "Use LineSegment2f or StraightLine2f")
typealias Line2f = Line<Vector2f>
typealias Line2d = Line<Vector2d>
typealias Line2i = Line<Vector2i>


@Serializable
data class Line<Point : Any>(val v0: Point, val v1: Point) : Polygon<Point> {

    override fun <TargetPoint : Any> convert(converter: (Point) -> TargetPoint): Line<TargetPoint> {
        return Line(
            converter(v0),
            converter(v1)
        )
    }

    override fun getVertex(index: Int): Point {
        return when (index) {
            0 -> v0
            1 -> v1
            else -> throw Error("Invalid vertex index")
        }
    }

    override fun getVertexAmount(): Int {
        return 2
    }

    override fun getEdge(index: Int): Line<Point> {
        if (index != 0) throw Error("Only zero valid index for line")
        return this
    }

    override fun getEdgeAmount(): Int {
        return 1
    }

    override fun getEdgeList(): List<Line<Point>> {
        return listOf(this)
    }

    companion object {
        fun <Point : AbstractVector<Point>> byDelta(pivot: Point, delta: Point): Line<Point> {
            return Line(pivot, pivot + delta)
        }

    }
}

