package ru.casperix.math.geometry

interface GeometryApi<Vector : Any, Component : Number> {
    fun getPointAroundRay(
        start: Vector,
        finish: Vector,
        point: Vector,
        error: Component
    ): PointAroundRay

    fun calculateDeterminant(start: Vector, finish: Vector, point: Vector): Component

    fun distPointToQuad(P: Vector, quad: Quad<Vector>): Component

    fun distPointToLine(P: Vector, T: Line<Vector>): Component

    fun distPointToSegment(P: Vector, A: Vector, B: Vector): Component

    fun projectionByDirection(source: Vector, direction: Vector): Vector

    fun projectionByDirectionLength(source: Vector, direction: Vector): Component

    fun distPointToSegment(P: Vector, segment: Line<Vector>): Component {
        return distPointToSegment(P, segment.v0, segment.v1)
    }


}