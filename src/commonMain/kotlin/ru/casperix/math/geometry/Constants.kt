package ru.casperix.math.geometry

import kotlin.math.E
import kotlin.math.PI

val EPSILON = 0.000001
val fEPSILON = 0.000001f

val RADIAN_TO_DEGREE = 180.0 / PI
val DEGREE_TO_RADIAN = PI / 180.0

val PI2 = PI * 2.0
val HPI = PI / 2.0

val MAX_DEGREE = 360.0

val fPI2 = PI2.toFloat()
val fPI = PI.toFloat()
val fHPI = HPI.toFloat()
val fE = E.toFloat()
val fMAX_DEGREE = 360f

val fRADIAN_TO_DEGREE = RADIAN_TO_DEGREE.toFloat()
val fDEGREE_TO_RADIAN = DEGREE_TO_RADIAN.toFloat()