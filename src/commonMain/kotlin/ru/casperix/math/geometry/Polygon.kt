package ru.casperix.math.geometry

import ru.casperix.math.collection.getLooped
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i
import kotlin.math.max

interface Polygon<Point : Any> {
    fun <TargetPoint : Any> convert(converter: (Point) -> TargetPoint): Polygon<TargetPoint>

    fun getVertices(): List<Point> {
        return (0 until getVertexAmount()).map { getVertex(it) }
    }

    fun getTriangle(index: Int): Triangle<Point> {
        val points = getVertices()
        if (points.size < 3) throw Error("Expected 3 or more vertices for search face. Actual: ${points.size}")
        if (index < 0 || index >= points.size - 2) throw Error("Expected index in range 0 - ${points.size - 3}. Actual: $index")

        return Triangle(points[0], points[index + 1], points[index + 2])
    }

    fun getTriangleAmount(): Int {
        val points = getVertices()
        return max(0, points.size - 2)
    }

    fun getVertex(index: Int): Point {
        val points = getVertices()
        return points.get(index)
    }

    fun getVertexAmount(): Int {
        val points = getVertices()
        return points.size
    }


    fun getEdge(index: Int): Line<Point> {
        val points = getVertices()
        if (points.size < 2) throw Error("Expected 2 or more vertices for search edge. Actual: ${points.size}")
        if (index < 0 || index >= points.size) throw Error("Expected index in range 0 - ${points.size - 1}. Actual: $index")

        return Line(points.getLooped(index), points.getLooped(index+1))

    }


    fun getEdgeAmount(): Int {
        val points = getVertices()
        if (points.size <= 1) return 0
        if (points.size == 2) return  1
        return points.size
    }

    fun getEdgeList(): List<Line<Point>> {
        return (0 until getEdgeAmount()).map {
            getEdge(it)
        }
    }

    @Deprecated(message = "Undefined behaviour for complex polygon")
    fun getTriangleList(): List<Triangle<Point>> {
        return (0 until getTriangleAmount()).map {
            getTriangle(it)
        }
    }
}

typealias CustomPolygon2f = CustomPolygon<Vector2f>

typealias Polygon3f = Polygon<Vector3f>
typealias Polygon2f = Polygon<Vector2f>
typealias Polygon3d = Polygon<Vector3d>
typealias Polygon2d = Polygon<Vector2d>
typealias Polygon3i = Polygon<Vector3i>
typealias Polygon2i = Polygon<Vector2i>


