package ru.casperix.math.mesh

import ru.casperix.math.mesh.component.RTreeEdge
import ru.casperix.math.mesh.component.RTreePoint
import ru.casperix.math.mesh.component.RTreeRegion
import ru.casperix.math.mesh.float32.Mesh

class RTreeMeshBuilder(source: Mesh = Mesh()) : MeshBuilder {
    override val points = RTreePoint(source.points)
    override val edges = RTreeEdge(source.edges)
    override val regions = RTreeRegion(source.regions)
}