package ru.casperix.math.mesh.component

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.float32.getAABBox
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.mesh.SpatialMap
import ru.casperix.math.mesh.float32.MeshRegion
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.toQuad
import ru.casperix.misc.contains
import com.github.davidmoten.rtree2.Entries
import com.github.davidmoten.rtree2.RTree
import com.github.davidmoten.rtree2.geometry.Rectangle

class RTreeRegion(initial: List<MeshRegion> = emptyList()) : SpatialMap<MeshRegion> {
    private var container = RTree.create(initial.map { Entries.entry(it, getRectangle(it)) })

    override fun clear() {
        container = RTree.create()
    }

    override fun all(): List<MeshRegion> {
        return container.entries().map { it.value() }
    }

    override fun has(value: MeshRegion): Boolean {
        val candidateList = search(value.shape.getAABBox())
        return candidateList.contains { it == value }
    }

    private fun getRectangle(value: MeshRegion): Rectangle {
        return value.shape.getAABBox().toRRectangle()
    }


    override fun add(element: MeshRegion) {
        container = container.add(element, getRectangle(element))
    }

    override fun remove(element: MeshRegion) {
        container = container.delete(element, getRectangle(element))
    }

    override fun search(area: LineSegment2f): Collection<MeshRegion> {
        val candidateList = search(area.getAABBox())
        return candidateList.filter { other ->
            Intersection2Float.hasPolygonWithSegment(other.shape, area)
        }
    }

    override fun search(area: Box2f): Collection<MeshRegion> {
        val self = area.toQuad()

        return container.search(area.toRRectangle()).filter { other ->
            Intersection2Float.hasPolygonWithPolygon(other.value().shape, self)
        }.map { it.value() }
    }

    override fun search(area: Circle2f): Collection<MeshRegion> {
        return container.search(area.toRCircle()).filter {
            Intersection2Float.hasPolygonWithCircle(it.value().shape, area)
        }.map { it.value() }
    }

    override fun search(area: Polygon2f): Collection<MeshRegion> {
        return container.search(area.getAABBox().toRRectangle()).filter {
            Intersection2Float.hasPolygonWithPolygon(it.value().shape, area)
        }.map { it.value() }

    }
}