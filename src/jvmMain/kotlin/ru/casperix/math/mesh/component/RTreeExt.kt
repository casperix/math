package ru.casperix.math.mesh.component

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import com.github.davidmoten.rtree2.geometry.*

fun Circle2f.toRCircle(): Circle {
    return Geometries.circle(center.x, center.y, range)
}

fun Box2f.toRRectangle(): Rectangle {
    return Geometries.rectangle(min.x, min.y, max.x, max.y)
}

fun Vector2f.toRPoint(): Point {
    return Geometries.point(x, y)
}

fun LineSegment2f.toRLine(): Line {
    return Geometries.line(start.x, start.y, finish.x, finish.y)
}

fun Rectangle.toBox2f(): Box2f {
    return Box2f(Vector2f(x1().toFloat(), y1().toFloat()), Vector2f(x2().toFloat(), y2().toFloat()))
}

fun Point.toVector2f(): Vector2f {
    return Vector2f(x().toFloat(), y().toFloat())
}

fun Line.toLineSegment2f(): LineSegment2f {
    return LineSegment2f(Vector2f(x1().toFloat(), y1().toFloat()), Vector2f(x2().toFloat(), y2().toFloat()))
}
