package ru.casperix.math.mesh.component

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.float32.getAABBox
import ru.casperix.math.curve.float32.Circle2f
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.mesh.SpatialMap
import ru.casperix.math.mesh.float32.MeshEdge
import ru.casperix.math.straight_line.float32.LineSegment2f
import com.github.davidmoten.rtree2.Entries
import com.github.davidmoten.rtree2.RTree

class RTreeEdge(initial: List<MeshEdge> = emptyList()) : SpatialMap<MeshEdge> {
    private var container = RTree.create(initial.map { Entries.entry(it, it.shape.toRLine()) })

    override fun clear() {
        container = RTree.create()
    }

    override fun all(): List<MeshEdge> {
        return container.entries().map { it.value() }
    }

    override fun add(element: MeshEdge) {
        container = container.add(element, element.shape.toRLine())
    }

    override fun remove(element: MeshEdge) {
        container = container.delete(element, element.shape.toRLine())
    }

    override fun has(element: MeshEdge): Boolean {
        val candidateList = search(element.shape)
        return candidateList.contains(element)
    }

    override fun search(area: LineSegment2f): Collection<MeshEdge> {
        return container.search(area.toRLine()).map { it.value() }
    }

    override fun search(area: Polygon2f): Collection<MeshEdge> {
        val candidateList = search(area.getAABBox())
        return candidateList.filter { other ->
            Intersection2Float.hasPolygonWithSegment(area, other.shape)
        }
    }

    override fun search(area: Box2f): Collection<MeshEdge> {
        return container.search(area.toRRectangle()).map { it.value() }
    }

    override fun search(area: Circle2f): Collection<MeshEdge> {
        return container.search(area.toRCircle()).map { it.value() }
    }


}