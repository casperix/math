package ru.casperix.map2d

import ru.casperix.math.array.float32.FloatMap2D
import ru.casperix.math.vector.int32.Vector2i
import kotlin.test.Test
import kotlin.test.assertEquals

class MatrixMultiplyTest {
    @Test
    fun test() {
        val A = FloatMap2D(
            Vector2i(4, 3), floatArrayOf(
            1f, 2f, 3f, 4f,
            5f, 6f, 7f, 8f,
            9f, 10f, 11f, 12f,
        ))
        val B = FloatMap2D(
            Vector2i(1, 4), floatArrayOf(
            1f,
            2f,
            3f,
            4f,
        ))

        val C = FloatMap2D(
            Vector2i(1, 3), floatArrayOf(
            1f * 1f + 2f * 2f + 3f * 3f + 4f * 4f,
            1f * 5f + 2f * 6f + 3f * 7f + 4f * 8f,
            1f * 9f + 2f * 10f + 3f * 11f + 4f * 12f,
        ))

        assertEquals(C, A * B)
    }

}