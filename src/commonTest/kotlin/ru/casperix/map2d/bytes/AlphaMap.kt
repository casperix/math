package ru.casperix.map2d.bytes

import ru.casperix.math.array.MutableMap2D
import ru.casperix.math.array.int8.*

data class AlphaMap(val bytes: ByteArray3D) : MutableMap2D<Alpha> by ByteBasedMap2D(bytes, AlphaCodec()) {
    constructor(width: Int, height: Int) : this(ByteArray3D(width, height, 4))
}

data class Alpha(val value: Float)

class AlphaCodec : ByteCodec<Alpha> {
//		for JVM only:
//	private val buffer = ByteBuffer.allocate(4)

    override fun decode(bytes: ByteArray): Alpha {
        val int = Int.fromBytes(bytes)

        val float = Float.fromBits(int)
        return Alpha(float)
//		for JVM only:
//		return Alpha(ByteBuffer.wrap(bytes).getFloat())
    }


    override fun encode(custom: Alpha): ByteArray {

        val bits = custom.value.toBits()
        val bytes = bits.toBytes()
        return bytes
//		for JVM only:
//		buffer.putFloat(0, custom.alpha)
//		return buffer.array()
    }


}
