package ru.casperix.map2d.bytes

import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.time.executeAndMeasure
import kotlin.test.Test

class PixelMapTest {
	@Test
	fun performanceWrite() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = PixelMap(size, size)
			for (x in 0 until bitmap.sizeX) {
				for (y in 0 until bitmap.sizeY) {
					write(bitmap, x, y)
				}
			}
			bitmap
		}

		println(bitmap.hashCode())
		println("time: $time ms; size: $size x $size; ")

	}

	private fun write(bitmap: PixelMap, x: Int, y: Int) {
		val pixel = Pixel(x.toByte(), y.toByte(), -128, 127)
		val position = Vector2i(x, y)
		bitmap.set(position, pixel)
	}


}