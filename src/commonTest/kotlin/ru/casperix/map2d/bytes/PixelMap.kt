package ru.casperix.map2d.bytes

import ru.casperix.math.array.int8.ByteBasedMap2D
import ru.casperix.math.array.int8.ByteArray3D
import ru.casperix.math.array.int8.ByteCodec
import ru.casperix.math.array.MutableMap2D
import kotlinx.serialization.Serializable

data class Pixel(val r: Byte, val g: Byte, val b: Byte, val a: Byte)

class PixelCodec : ByteCodec<Pixel> {
	override fun decode(bytes: ByteArray): Pixel {
		return Pixel(bytes[0], bytes[1], bytes[2], bytes[3])
	}

	override fun encode(custom: Pixel): ByteArray {
		return byteArrayOf(custom.r, custom.g, custom.b, custom.a)
	}

}

@Serializable
data class PixelMap(val bytes: ByteArray3D) : MutableMap2D<Pixel> by ByteBasedMap2D(bytes, PixelCodec()) {
	constructor(width: Int, height: Int) : this(ByteArray3D(width, height, 4))
}