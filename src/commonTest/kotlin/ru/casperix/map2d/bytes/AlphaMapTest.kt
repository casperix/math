package ru.casperix.map2d.bytes

import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.time.executeAndMeasure
import kotlin.test.Test
import kotlin.test.assertEquals

class AlphaMapTest {
	@Test
	fun check() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = AlphaMap(size, size)
			for (x in 0 until bitmap.sizeX) {
				for (y in 0 until bitmap.sizeY) {
					write(bitmap, x, y)
				}
			}
			for (x in 0 until bitmap.sizeX) {
				for (y in 0 until bitmap.sizeY) {
					read(bitmap, x, y)
				}
			}
			bitmap
		}

		println("check-hash-code:${bitmap.hashCode()}")
		println("time: $time ms; size: $size x $size; ")

	}


	private fun writeAndRead(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		bitmap.set(Vector2i(x, y), value)

		val actualValue = bitmap.get(Vector2i(x, y))
		assertEquals(value, actualValue)
	}

	private fun read(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		val actualValue = bitmap.get(Vector2i(x, y))
		assertEquals(value, actualValue)
	}

	@Test
	fun performanceWrite() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val bitmap = AlphaMap(size, size)
			for (x in 0 until bitmap.sizeX) {
				for (y in 0 until bitmap.sizeY) {
					write(bitmap, x, y)
				}
			}
			bitmap
		}

		println("performance-hash-code:${bitmap.hashCode()}")
		println("time: $time ms; size: $size x $size; ")

	}

	private fun write(bitmap: AlphaMap, x: Int, y: Int) {
		val value = Alpha(x.toFloat() / y.toFloat())
		bitmap.set(Vector2i(x, y), value)
	}


}