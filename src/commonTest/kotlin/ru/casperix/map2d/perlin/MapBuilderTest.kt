package ru.casperix.map2d.perlin

import ru.casperix.math.array.float64.DoubleMap2D
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.time.executeAndMeasure
import kotlin.test.Test

class MapBuilderTest {
	@Test
	fun createMapXY() {
		val dimension = Vector2i(4096)
		val (time, map) = executeAndMeasure {
			DoubleMap2D.createByXY(dimension) { pos ->
				(pos.x + pos.y).toDouble()
			}
		}
		println("Map $dimension for $time ms")
	}
}