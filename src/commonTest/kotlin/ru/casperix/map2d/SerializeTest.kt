package ru.casperix.map2d

import ru.casperix.math.array.int8.ByteMap2D
import ru.casperix.math.array.float64.DoubleMap2D
import ru.casperix.math.array.int32.IntMap2D
import ru.casperix.math.array.int16.ShortMap2D
import ru.casperix.math.vector.int32.Vector2i
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test

/**
 *
 */
class SerializeTest {
	val a1 = ShortMap2D(Vector2i.ONE, shortArrayOf(19))
	val a2 = IntMap2D(Vector2i.ONE, intArrayOf(19))
	val a3 = ByteMap2D(Vector2i.ONE, byteArrayOf(19))
	val a4 = DoubleMap2D(Vector2i.ONE, doubleArrayOf(19.0))
//	val a5 = CustomMap2D(Vector2i.ONE, mutableListOf("value"))
//	val a6 = mutableMap2d(Vector2i.ONE, { "mutable" })
//	val a7 = array2d(Vector2i.ONE, { "immutable" })

	@Test
	fun simple() {
		val JSON = Json {  allowStructuredMapKeys = true; prettyPrint = true }
//		val PROTOBUF = ProtoBuf { encodeDefaults = false }

		val res1 = JSON.encodeToString(a1)
		val res2 = JSON.encodeToString(a2)
		val res3 = JSON.encodeToString(a3)
		val res4 = JSON.encodeToString(a4)
//		val res5 = JSON.encodeToString(a5)
//		val res6 = JSON.encodeToString(a6)
//		val res7 = JSON.encodeToString(a7)

		println("success")
//			val protoOutput = PROTOBUF.encodeToByteArray(item)


	}
}