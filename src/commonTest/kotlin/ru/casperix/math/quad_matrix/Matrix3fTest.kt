package ru.casperix.math.quad_matrix

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.test.FloatTest
import kotlin.test.Test
import kotlin.test.assertEquals

class Matrix3fTest {

    @Test
    fun transpose() {
        val source = FloatArray(9) { it.toFloat() }

        val m = Matrix3f(source)
        assertEquals(
            Matrix3f(
                floatArrayOf(
                    0f, 3f, 6f,
                    1f, 4f, 7f,
                    2f, 5f, 8f,
                )
            ),
            m.transpose()
        )
    }

    @Test
    fun mulIdentity() {
        val m = Matrix3f.IDENTITY * Matrix3f.IDENTITY
        assertEquals(Matrix3f.IDENTITY, m)
    }

    @Test
    fun mul() {
        val m1 = Matrix3f(FloatArray(9) { (it + 1).toFloat() })
        val m2 = Matrix3f(FloatArray(9) { (it + 10).toFloat() })

        val expected = Matrix3f(
            floatArrayOf(
                84f, 90f, 96f,
                201f, 216f, 231f,
                318f, 342f, 366f,
            )
        )
        val actual = m1 * m2

        FloatTest.assertEquals(expected, actual)
    }

}