package ru.casperix.math

import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.curve.float32.Arc2f
import ru.casperix.math.geometry.fPI2
import ru.casperix.math.test.FloatTest
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals

class ArcTest {
    @Test
    fun length() {
        val arc = Arc2f(Vector2f.ZERO, RadianFloat.ZERO, RadianFloat.PI2, 1f)
        FloatTest.assertEquals(fPI2, arc.length())

        val arc2 = Arc2f(Vector2f.ZERO, RadianFloat.ZERO, -RadianFloat.PI2, 1f)
        FloatTest.assertEquals(fPI2, arc2.length())
    }

    @Test
    fun positiveInside() {
        val arc = Arc2f.byRadian(Vector2f.ZERO, 0f, 1f, 1f)

        assertEquals(true, arc.isAngleInside(RadianFloat(0.0f)))
        assertEquals(true, arc.isAngleInside(RadianFloat(0.5f)))
        assertEquals(true, arc.isAngleInside(RadianFloat(1.0f)))
        assertEquals(false, arc.isAngleInside(RadianFloat(-0.1f)))
        assertEquals(false, arc.isAngleInside(RadianFloat(1.1f)))

        val arc2 = Arc2f.byRadian(Vector2f.ZERO, -1f, 1f, 1f)

        assertEquals(true, arc2.isAngleInside(RadianFloat(-0.0f)))
        assertEquals(true, arc2.isAngleInside(RadianFloat(-0.5f)))
        assertEquals(true, arc2.isAngleInside(RadianFloat(-1.0f)))
        assertEquals(false, arc2.isAngleInside(RadianFloat(0.1f)))
        assertEquals(false, arc2.isAngleInside(RadianFloat(-1.1f)))
    }

    @Test
    fun negativeInside() {
        val arc = Arc2f.byRadian(Vector2f.ZERO, 0f, -1f, 1f)

        assertEquals(true, arc.isAngleInside(RadianFloat(0.0f)))
        assertEquals(true, arc.isAngleInside(RadianFloat(-0.5f)))
        assertEquals(true, arc.isAngleInside(RadianFloat(-1.0f)))
        assertEquals(false, arc.isAngleInside(RadianFloat(0.1f)))
        assertEquals(false, arc.isAngleInside(RadianFloat(-1.1f)))

        val arc2 = Arc2f.byRadian(Vector2f.ZERO, 1f, -1f, 1f)

        assertEquals(true, arc2.isAngleInside(RadianFloat(0.0f)))
        assertEquals(true, arc2.isAngleInside(RadianFloat(0.5f)))
        assertEquals(true, arc2.isAngleInside(RadianFloat(1.0f)))
        assertEquals(false, arc2.isAngleInside(RadianFloat(-0.1f)))
        assertEquals(false, arc2.isAngleInside(RadianFloat(1.1f)))
    }
}