package ru.casperix.math

import ru.casperix.misc.randomItem
import kotlin.random.Random
import kotlin.test.Test

class RandomTest {

	@Test
	fun randomTest() {
		val items = listOf(Pair(0, 1.0), Pair(1, 3.0))

		val random = Random(0)

		var checkSumm = 0
		for (i in 1..1000000) {
			val item = randomItem(random, items)
			if (item != null) {
				checkSumm += item
			}
		}
		println(checkSumm)
	}
}