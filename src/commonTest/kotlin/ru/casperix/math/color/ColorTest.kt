package ru.casperix.math.color

import ru.casperix.math.color.misc.ColorDecoder
import ru.casperix.math.color.rgba.RgbaColor1i
import ru.casperix.math.test.FloatTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ColorTest {

    @Test
    fun convertRGB_HSV() {
        Color.all.forEach {
            val rgbExpect = it.toRGB()
            val hsv = rgbExpect.toHSV()
            val rgbActual = hsv.toRGB()

            FloatTest.assertEquals(rgbExpect.toVector3f(), rgbActual.toVector3f())
        }
    }

    @Test
    fun parseHex() {
        val A = ColorDecoder.parseHex("FF0201")
        assertEquals(RgbaColor1i(255u, 2u, 1u, 255u), A)

        val B = ColorDecoder.parseHex("FF020103")
        assertEquals(RgbaColor1i(255u, 2u, 1u, 3u), B)
    }

    @Test
    fun floatColor() {
        val A = RgbaColor1i(250u, 251u, 252u, 255u)
        val B = A.toColor1f()
        val C = B.toColor1i()

        assertEquals(A, C)
    }

}