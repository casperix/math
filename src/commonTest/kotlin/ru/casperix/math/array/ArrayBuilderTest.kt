package ru.casperix.math.array

import ru.casperix.math.array.float32.FloatArrayBuilder
import ru.casperix.math.array.float32.UIntArrayBuilder
import kotlin.test.Test
import kotlin.test.assertContentEquals

class ArrayBuilderTest {
    @Test
    fun floats() {
        val builder = FloatArrayBuilder(0)
        assertContentEquals(floatArrayOf(), builder.build())

        builder.append(floatArrayOf(0f))
        assertContentEquals(floatArrayOf(0f), builder.build())

        builder.append(floatArrayOf(1f, 2f))
        assertContentEquals(floatArrayOf(0f, 1f, 2f), builder.build())

        builder.append(floatArrayOf(3f, 4f, 5f))
        assertContentEquals(floatArrayOf(0f, 1f, 2f, 3f, 4f, 5f), builder.build())
    }

    @Test
    fun uints() {
        val builder = UIntArrayBuilder(0)
        assertContentEquals(uintArrayOf(), builder.build())

        builder.append(uintArrayOf(0u))
        assertContentEquals(uintArrayOf(0u), builder.build())

        builder.append(uintArrayOf(1u, 2u))
        assertContentEquals(uintArrayOf(0u, 1u, 2u), builder.build())

        builder.append(uintArrayOf(3u, 4u, 5u))
        assertContentEquals(uintArrayOf(0u, 1u, 2u, 3u, 4u, 5u), builder.build())
    }
}