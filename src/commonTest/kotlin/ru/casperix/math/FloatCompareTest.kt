package ru.casperix.math

import ru.casperix.math.test.FloatCompare.unique
import ru.casperix.math.test.FloatCompare.uniqueOrdered
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals

class FloatCompareTest {
    @Test
    fun unique() {
        val items = listOf(Vector2f(1.000f), Vector2f(1.100f), Vector2f(1.010f), Vector2f(1.001f))

        val list4 = items.unique(0.0005f)
        val list3 = items.unique(0.005f)
        val list2 = items.unique(0.05f)
        val list1 = items.unique(0.5f)

        assertEquals(4, list4.size, "$list4")
        assertEquals(3, list3.size, "$list3")
        assertEquals(2, list2.size, "$list2")
        assertEquals(1, list1.size, "$list2")
    }

    @Test

    fun uniqueOrdered() {
        val source = listOf(
            Vector2f(0f, 1f),
            Vector2f(2f, 4f),
            Vector2f(1f, 3f),
            Vector2f(1f, 3f),
            Vector2f(1f, 3.00005f),
            Vector2f(0f, 1f),
            Vector2f(1f, 1f),
        )
        val target = listOf(
            Vector2f(0f, 1f),
            Vector2f(2f, 4f),
            Vector2f(1f, 3f),
            Vector2f(0f, 1f),
            Vector2f(1f, 1f),
        )
        assertEquals(target, source.uniqueOrdered())
    }
}