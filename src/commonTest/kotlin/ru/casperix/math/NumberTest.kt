package ru.casperix.math

import ru.casperix.misc.sliceSafe
import ru.casperix.misc.toPrecision
import kotlin.test.Test
import kotlin.test.assertEquals

class NumberTest {

    @Test
    fun toPrecisionTest() {
        val text = "HELLO"

        (0..4).forEach {
            println("INDEX: $it")
            val selectionIndices = it..it

            print(text.sliceSafe(0 until selectionIndices.first))
            print("-")
            print(text.sliceSafe(selectionIndices))
            print("-")
            print(text.sliceSafe(selectionIndices.last + 1 until text.length))
            print("\n")
        }


        assertEquals("-1", (-1.5).toPrecision(0))
        assertEquals("-1", (-1.1).toPrecision(0))
        assertEquals("0", (-0.1).toPrecision(0))
        assertEquals("0", (0.1).toPrecision(0))
        assertEquals("1", (0.9).toPrecision(0))
        assertEquals("2", (1.5).toPrecision(0))

        assertEquals("-1.6", (-1.59).toPrecision(1))
        assertEquals("0.0", (-0.02).toPrecision(1))
        assertEquals("0.0", (0.02).toPrecision(1))
        assertEquals("1.6", (1.59).toPrecision(1))

        assertEquals("0", Double.MIN_VALUE.toPrecision(0))
        assertEquals("-∞", Double.NEGATIVE_INFINITY.toPrecision(0))
        assertEquals("+∞", Double.MAX_VALUE.toPrecision(0))
        assertEquals("+∞", Double.POSITIVE_INFINITY.toPrecision(0))
        assertEquals("∞", Double.NaN.toPrecision(0))
    }
}