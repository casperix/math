package ru.casperix.math.geometry

import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals

class Intersection2fTest {
    val smallValue = 0.000001f

    @Test
    fun pointWithTriangle() {
        val testTriangle = Triangle2f(
            Vector2f(1f, 1f),
            Vector2f(3f, 1f),
            Vector2f(1f, 3f),
        )

        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(1f - smallValue, 1f), testTriangle))
        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(1f, 1f - smallValue), testTriangle))
        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(1f - smallValue), testTriangle))

        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(1f, 1f+smallValue), testTriangle))
        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(1f+smallValue, 1f), testTriangle))
        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(1f+smallValue), testTriangle))

        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(3f - smallValue, 1f), testTriangle))
        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(3f + smallValue, 1f), testTriangle))

        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(1f, 3f - smallValue), testTriangle))
        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(1f, 3f + smallValue), testTriangle))

        assertEquals(true, Intersection2Float.hasPointWithTriangle(Vector2f(2f - smallValue), testTriangle))
        assertEquals(false, Intersection2Float.hasPointWithTriangle(Vector2f(2f + smallValue), testTriangle))
    }

    @Test
    fun pointWithQuad() {
        val testQuad = Quad2f(
            Vector2f(1f, 1f),
            Vector2f(3f, 1f),
            Vector2f(3f, 3f),
            Vector2f(1f, 3f),
        )

        assertEquals(false, Intersection2Float.hasPointWithQuad(Vector2f(1f - smallValue), testQuad))
        assertEquals(false, Intersection2Float.hasPointWithQuad(Vector2f(3f + smallValue), testQuad))
        assertEquals(false, Intersection2Float.hasPointWithQuad(Vector2f(3f, 1f-smallValue), testQuad))
        assertEquals(false, Intersection2Float.hasPointWithQuad(Vector2f(1f-smallValue, 3f), testQuad))

        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(1f, 1f), testQuad))
        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(2f, 2f), testQuad))
        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(3f, 3f), testQuad))

        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(1f, 3f), testQuad))
        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(2f, 2f), testQuad))
        assertEquals(true, Intersection2Float.hasPointWithQuad(Vector2f(3f, 1f), testQuad))

    }
}