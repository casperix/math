package ru.casperix.math.geometry

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.geometry.builder.Triangulator
import ru.casperix.math.vector.toQuad
import kotlin.test.Test
import kotlin.time.measureTime

class TriangulatorTest {
    @Test
    fun performance() {
        val res = Triangulator.polygonWithContour(Box2f.ONE.toQuad(), 0.1f)
        println("TEST:" + res.body.size+ " / " + res.border.size)

        val time = measureTime {
            repeat(1000_000) {
               Triangulator.polygonWithContour(Box2f.ONE.toQuad(), 0.1f)

            }
        }

        println("ms: ${time.inWholeMilliseconds}")
    }
}