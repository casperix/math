package ru.casperix.math.geometry

import ru.casperix.misc.toPrecision
import ru.casperix.math.quaternion.float64.QuaternionDouble
import ru.casperix.math.iteration.Line3Iterator
import ru.casperix.math.intersection.*
import ru.casperix.math.transform.float64.Transform3d
import ru.casperix.math.axis_aligned.float64.Box3d
import ru.casperix.math.geometry.float64.Sphere3d
import ru.casperix.math.vector.float64.Vector3d
import kotlin.math.PI
import kotlin.random.Random
import kotlin.test.*

fun Random.nextVector3d(min: Double = 0.0, max: Double = 1.0): Vector3d {
	return Vector3d(
		this.nextDouble(min, max),
		this.nextDouble(min, max),
		this.nextDouble(min, max)
	)
}

class IntersectionTest {

	@Test
	fun testOctagonWithOctagon() {
		val a1 = Octagon3d(Vector3d.ZERO, Vector3d(1.0, 2.0, 4.0))
		val a2 = a1.convert { it + Vector3d(1.01) }
		val a3 = a1.convert { it + Vector3d(0.5) }
		assertTrue { intersectionOctagonWithOctagon(a1, a2) == null }
		assertTrue { intersectionOctagonWithOctagon(a1, a3) != null }

		val t1 = Transform3d(Vector3d(-0.4, 0.4, 0.0), Vector3d.ONE, QuaternionDouble.fromAxisAnge(Vector3d.Z, PI / 4.0))
		val t2 = Transform3d(Vector3d(-0.6, 0.6, 0.0), Vector3d.ONE, QuaternionDouble.fromAxisAnge(Vector3d.Z, PI / 4.0))
		val b1 = Octagon3d(Vector3d.ZERO, Vector3d.ONE)
		val b2 = Octagon3d(Vector3d.ZERO, Vector3d.ONE).convert { t1.getMatrix().transform(it) }
		val b3 = Octagon3d(Vector3d.ZERO, Vector3d.ONE).convert { t2.getMatrix().transform(it) }
		assertTrue { intersectionOctagonWithOctagon(b1, b2) != null }
		assertTrue { intersectionOctagonWithOctagon(b1, b3) == null }
	}

	@Test
	fun testBoxWithBox() {
		val main = Box3d(-Vector3d.ONE, Vector3d.ONE)

		val aYes = Box3d(main.min - Vector3d.ONE, main.min + Vector3d.ONE * EPSILON)
		val aNo = Box3d(main.min - Vector3d.ONE, main.min - Vector3d.ONE * EPSILON)

		val bYes = Box3d(main.max - Vector3d.ONE * EPSILON, main.max + Vector3d.ONE * 9.0)
		val bNo = Box3d(main.max + Vector3d.ONE * EPSILON, main.max + Vector3d.ONE * 10.0)

		assertTrue { hasIntersectionBoxWithBox(main, aYes) }
		assertTrue { hasIntersectionBoxWithBox(main, bYes) }
		assertTrue { getPenetrationBoxWithBox(main, aYes) != null }
		assertTrue { getPenetrationBoxWithBox(main, bYes) != null }

		assertFalse { hasIntersectionBoxWithBox(main, aNo) }
		assertFalse { hasIntersectionBoxWithBox(main, bNo) }
		assertFalse { getPenetrationBoxWithBox(main, aNo) != null }
		assertFalse { getPenetrationBoxWithBox(main, bNo) != null }

		val vYes = listOf(Pair(-3.0, -1 + EPSILON), Pair(1 - EPSILON, 3.0), Pair(-3.0, 3.0), Pair(-3.0, 30.0))
		val vNo = listOf(Pair(-3.0, -1 - EPSILON), Pair(1 + EPSILON, 3.0), Pair(-30.0, -20.0), Pair(2.0, 100.0))
		for (index in 0..2) {
			vYes.forEach {
				subTest(main, it, index, true)
			}
			vNo.forEach {
				subTest(main, it, index, false)
			}
		}

	}

	private fun subTest(main: Box3d, it: Pair<Double, Double>, index: Int, expected: Boolean) {
		val other = if (index == 0) {
			Box3d(main.min.copy(x = it.first), main.max.copy(x = it.second))
		} else if (index == 1) {
			Box3d(main.min.copy(y = it.first), main.max.copy(y = it.second))
		} else {
			Box3d(main.min.copy(z = it.first), main.max.copy(z = it.second))
		}
		assertEquals(expected, hasPenetrationBoxWithBox(main, other))
	}

	@Test
	fun sphereWithBoxTest1() {
		val sphere = Sphere3d(Vector3d.ONE * 3.0, 1.001)
		val box = Box3d(Vector3d.ONE, Vector3d.ONE + Vector3d(2.5, 2.5, 1.0))
		val info = intersectionBoxWithSphere(box, sphere)

		assertNotNull(info)
		assertEquals(info.delta().normalize().toPrecision(4), Vector3d.Z.toPrecision(4))
	}

	@Test
	fun sphereWithBoxTest2() {
		val sphere = Sphere3d(Vector3d.ONE * 2.0, 1.001)
		val box = Box3d(Vector3d(0.0, 0.0, 3.0), Vector3d(2.5, 2.5, 4.0))
		val info = intersectionBoxWithSphere(box, sphere)

		assertNotNull(info)
		assertEquals(info.delta().normalize().toPrecision(4), (-Vector3d.Z).toPrecision(4))
	}

	@Test
	fun sphereWithBoxTest3() {
		val sphere = Sphere3d(Vector3d(2.5, 2.5, 4.0), 0.501)
		val box = Box3d(Vector3d(0.0, 0.0, 0.0), Vector3d(2.5, 2.5, 3.5))
		val info = intersectionBoxWithSphere(box, sphere)

		assertNotNull(info)
	}

	@Test
	fun lineDrawTest() {
		var index = 0
		Line3Iterator.iterate3n(Line3d(Vector3d(0.001, 0.001, 0.001), Vector3d(10.0, 10.0, 10.0))) {
			println("index: ${index++}; pos:$it")
			false
		}
	}

	/**
	 * Создаем простой базовый треугольник
	 * Масштабируем его в чуть меньший и в чуть больший:
	 * На основе меньшего создаем лучи которые заведомо пересекут базовый
	 * На основе большего создаем лучи которые заведомо НЕ пересекут базовый
	 */
	@Test
	fun triangleTest() {
		testWith(Vector3d.ZERO, Vector3d.X, Vector3d.Z, arrayOf(Vector3d.Y, Vector3d.XY, Vector3d.YZ, Vector3d.ONE))
		testWith(Vector3d.ZERO, Vector3d.Y, Vector3d.Z, arrayOf(Vector3d.X, Vector3d.XY, Vector3d.XZ, Vector3d.ONE))
		testWith(Vector3d.ZERO, Vector3d.X, Vector3d.Y, arrayOf(Vector3d.Z, Vector3d.XZ, Vector3d.YZ, Vector3d.ONE))

		val random = Random(0)
		for (step in 1..10000) {
			testWith(random.nextVector3d(), random.nextVector3d(), random.nextVector3d(), arrayOf(Vector3d.ONE))
		}
	}

	private fun testWith(v0: Vector3d, v1: Vector3d, v2: Vector3d, offsets: Array<Vector3d>) {
		val small = 0.000001
		val basicTriangle = Triangle3d(v0, v1, v2)
		val smallTriangle = basicTriangle.scale( 1 - small)
		val bigTriangle = basicTriangle.scale( 1 + small)

		val normal = getTriangleNormal(basicTriangle.v0, basicTriangle.v1, basicTriangle.v2)

		for (vertexId in 0..2) {
			for (offset in offsets) {
				val volume = normal.cross(offset).length()

				val basicVertex = basicTriangle.getVertex(vertexId)
				val smallIntersection = intersectionLineWithTriangle(Line3d(smallTriangle.getVertex(vertexId) - offset, smallTriangle.getVertex(vertexId) + offset), basicTriangle)
				assertTrue(
					smallIntersection != null && (smallIntersection - basicVertex).length() < small,
					"Intersection with small triangle not founded" +
							"\nvertex: $vertexId" +
							"\ntriangle: $basicTriangle" +
							"\nt.normal: $normal" +
							"\nvolume: ${volume.toPrecision(4)}"
				)

				val bigIntersection = intersectionLineWithTriangle(Line3d(bigTriangle.getVertex(vertexId) - offset, bigTriangle.getVertex(vertexId) + offset), basicTriangle)
				assertTrue(bigIntersection == null, "Intersection with big triangle, vertex $vertexId must be invalid")
			}
		}
	}


}