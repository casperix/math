package ru.casperix.math.geometry


import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i
import kotlin.test.Test
import kotlin.test.assertFails
import kotlin.test.assertTrue

class Box2iTest {
	@Test
	fun test() {
		val points = listOf(Vector2i(0, 0), Vector2i(4, 2), Vector2i(4, -2), Vector2i(-4, -2))
		val validSizes = listOf(Vector2i(0, 0), Vector2i(1, 1), Vector2i(0, 1), Vector2i(1, 0), Vector2i(2, 4), Vector2i(4, 2))
		val invalidSizes = listOf(Vector2i(-1, -1), Vector2i(0, -1), Vector2i(-1, 0), Vector2i(-2, 0), Vector2i(0, -2), Vector2i(-2, -4))

		validSizes.forEach { size ->
			points.forEach { start ->
				assertTrue { null != Box2i.createOrNull(start, start + size) }
				val box = Box2i(start, start + size)
				assertTrue { box.min == start }
				assertTrue { box.max == start + size }
			}
		}


		invalidSizes.forEach { size ->
			points.forEach { start ->
				assertTrue { null == Box2i.createOrNull(start, start + size) }
				assertFails { Box2i(start, start + size) }
			}
		}
	}
}