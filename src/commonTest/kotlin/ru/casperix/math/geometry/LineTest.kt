package ru.casperix.math.geometry

import ru.casperix.math.geometry.float32.normal
import ru.casperix.math.test.FloatTest
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.math.vector.int32.Vector4i
import kotlin.test.Test

class LineTest {
    /**
     * extensions must work for any line, that implement [casperix.math.operator.VectorArithmetic]
     */
    @Test
    fun validExtensions() {
        val a = Line2f(Vector2f.ZERO, Vector2f.ONE)
        val b = Line2d(Vector2d.ZERO, Vector2d.ONE)
        val c = Line3d(Vector3d.ZERO, Vector3d.ONE)
        val d = Line(Vector4i.ZERO, Vector4i.ONE)

        a.length()
        b.length()
        c.length()
        d.length()

        a.delta()
        b.delta()
        c.delta()
        d.delta()
    }

    @Test
    fun normalAndTangent() {
        val line1 = Line2f(Vector2f(2f, 1f), Vector2f(4f, 1f))

        FloatTest.assertEquals(Vector2f(0f, 1f), line1.normal())
        FloatTest.assertEquals(Vector2f(1f, 0f), line1.tangent())

        val line2 = Line2f(Vector2f(2f, 1f), Vector2f(6f, 5f))

        FloatTest.assertEquals(Vector2f(-1f, 1f).normalize(), line2.normal())
        FloatTest.assertEquals(Vector2f(1f, 1f).normalize(), line2.tangent())
    }

}
