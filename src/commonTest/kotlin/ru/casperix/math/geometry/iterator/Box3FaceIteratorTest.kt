package ru.casperix.math.geometry.iterator

import ru.casperix.math.iteration.Box3FaceIterator
import ru.casperix.math.axis_aligned.int32.Box3i
import ru.casperix.math.vector.int32.Vector3i
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class Box3FaceIteratorTest {
	@Test
	fun simpleIterator() {
		val min = Vector3i(1, 2, 2)
		val max = Vector3i(6, 5, 4)
		val box = Box3i(min, max)
		val iterator = Box3FaceIterator(box)
		var counter = 0
		iterator.forEach {
			counter++
			assertTrue("Position: $it is not side") { box.isSide(it) }
		}
		assertEquals(64, counter)
	}

	@ExperimentalTime
	@Test
	fun benchmarkTest() {
		var checkSum = 0
		val time = measureTime {
			for (i in 1..1000) {
				val box = Box3i(Vector3i(11, 22, 33), Vector3i(44, 55, 66))
				Box3FaceIterator(box).forEach {
					checkSum += it.x
				}
			}
		}
		println("Iterated for: ${time.inWholeMilliseconds} ms")
		assertTrue { time.inWholeMilliseconds < 1000 }
	}
}