package ru.casperix.math.geometry.iterator

import ru.casperix.math.iteration.Box3FaceIterator
import ru.casperix.math.iteration.Box3Iterator
import ru.casperix.math.iteration.Circular3Iterator
import ru.casperix.math.axis_aligned.int32.Box3i
import ru.casperix.math.vector.int32.Vector3i
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class Box3IteratorTest {
	val boxList = listOf(Box3i(Vector3i(1, 2, 3), Vector3i(6, 5, 4)), Box3i(Vector3i(-3, -2, -1), Vector3i(6, 5, 4)))

	@Test
	fun manual() {
		checkManualIterator(Box3i(Vector3i.ONE, Vector3i.ONE), setOf(Vector3i.ONE))
		checkManualIterator(Box3i(Vector3i.ZERO, Vector3i.ZERO), setOf(Vector3i.ZERO))
		checkManualIterator(Box3i(-Vector3i.ONE, -Vector3i.ONE), setOf(-Vector3i.ONE))

		checkManualIterator(
			Box3i(Vector3i.ZERO, Vector3i.ONE), setOf(
				Vector3i(0, 0, 1), Vector3i(0, 1, 0), Vector3i(0, 1, 1), Vector3i(0, 0, 0),
				Vector3i(1, 0, 1), Vector3i(1, 1, 0), Vector3i(1, 1, 1), Vector3i(1, 0, 0)
			))
	}

	@Test
	fun checkBox() {
		boxList.forEach {
			checkBoxIterator(it)
		}
	}

	@Test
	fun checkFace() {
		boxList.forEach {
			checkFaceIterator(it)
		}
	}

	@Test
	fun checkFromCenter() {
		checkFromCenterIterator(Vector3i(1, 2, 3), 0)
		checkFromCenterIterator(Vector3i(1, 2, 3), 1)
		checkFromCenterIterator(Vector3i(1, 2, 3), 4)
	}

	private fun checkManualIterator(value: Box3i, result: Set<Vector3i>) {
		val positions = mutableSetOf<Vector3i>()

		Box3Iterator(value).forEach {
			positions.add(it)
		}

		assertEquals(result, positions, "Invalid")

	}

	private fun checkBoxIterator(value: Box3i) {
		val positions = mutableSetOf<Vector3i>()
		for (x in value.min.x..value.max.x) {
			for (y in value.min.y..value.max.y) {
				for (z in value.min.z..value.max.z) {
					positions.add(Vector3i(x, y, z))
				}
			}
		}
		val positionsTest = mutableSetOf<Vector3i>()

		Box3Iterator(value).forEach {
			positionsTest.add(it)
		}

		assertEquals(positions, positionsTest, "Invalid iterable positions")
	}

	private fun checkFromCenterIterator(center: Vector3i, radius: Int) {
		val templatePositions = mutableSetOf<Vector3i>()
		val min = center - Vector3i(radius, radius, radius)
		val max = center + Vector3i(radius, radius, radius)
		for (x in min.x..max.x) {
			for (y in min.y..max.y) {
				for (z in min.z..max.z) {
					templatePositions.add(Vector3i(x, y, z))
				}
			}
		}
		val positions = mutableSetOf<Vector3i>()
		val orders = mutableListOf<Vector3i>()

		Circular3Iterator(center, 0, radius).forEach {
			positions.add(it)
			orders.add(it)
		}

		for (i in 1 until orders.size) {
			val last = orders.get(i - 1)
			val next = orders.get(i)
			val lastDest = (center - last).absoluteMaximum()
			val nextDest = (center - next).absoluteMaximum()
			assertTrue(lastDest <= nextDest, "invalid order")
		}
		assertEquals(templatePositions, positions, "Invalid iterable positions")
	}

	private fun checkFaceIterator(value: Box3i) {
		val positions = mutableSetOf<Vector3i>()
		for (x in value.min.x..value.max.x) {
			for (y in value.min.y..value.max.y) {
				for (z in value.min.z..value.max.z) {
					if (value.isSide(Vector3i(x, y, z))) {
						positions.add(Vector3i(x, y, z))
					}
				}
			}
		}
		val positionsTest = mutableSetOf<Vector3i>()

		Box3FaceIterator(value).forEach {
			positionsTest.add(it)
		}

		assertEquals(positions, positionsTest, "Invalid iterable positions")
	}
}