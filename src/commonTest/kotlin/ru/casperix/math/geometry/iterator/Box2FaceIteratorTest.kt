package ru.casperix.math.geometry.iterator

import ru.casperix.math.iteration.Box2FaceIterator

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class Box2FaceIteratorTest {
	@Test
	fun simpleIterator() {
		val min = Vector2i(1, 2)
		val max = Vector2i(6, 5)
		val box = Box2i(min, max)
		val iterator = Box2FaceIterator(box)
		var counter = 0
		iterator.forEach {
			counter++
			assertTrue("Position: $it is not side") { box.isSide(it) }
		}
		assertEquals(16, counter)
	}

	@ExperimentalTime
	@Test
	fun benchmarkTest() {
		var checkSum = 0
		val time = measureTime {
			for (i in 1..1000) {
				val box = Box2i(Vector2i(11, 22), Vector2i(44, 55))
				Box2FaceIterator(box).forEach {
					checkSum += it.x
				}
			}
		}
		println("Iterated for: ${time.inWholeMilliseconds} ms")
		assertTrue { time.inWholeMilliseconds < 1000 }
	}
}