package ru.casperix.math.geometry

import ru.casperix.math.vector.float64.Vector2d
import kotlin.test.Test
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@OptIn(ExperimentalTime::class)
class PerformanceTest {
	val COUNT = 1000_000

	@Test
	fun create() {
		var r = Vector2d(0.0, 0.0)
		val d = measureTime {
			for (i in 1..COUNT) {
				val next = Vector2d(i.toDouble(), i.toDouble())
				r += next
			}
		}

		//19ms
		println("result: $r\ntime:$d")

	}
}