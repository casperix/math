package ru.casperix.math.curve

import ru.casperix.math.curve.float32.BezierCubic2f
import ru.casperix.math.curve.float32.BezierQuadratic2f
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.time.measureTime

class CurvePerformanceTest {
    @Test
    fun test() {

        val list = listOf(
            BezierCubic2f(Vector2f(0f, 0f), Vector2f(0f, 1f), Vector2f(1f, 0f), Vector2f(0f, -1f)),
            BezierQuadratic2f(Vector2f(0f, 0f), Vector2f(1f, 2f), Vector2f(2f, 0f)),
        )

        list.forEach { curve ->
            val steps = 1000
            var position = Vector2f.ZERO
            val time = measureTime {
                repeat(steps) {
                    repeat(steps) {
                        val index = it.toFloat() / steps
                        position += curve.getPosition(index)
                    }
                }
            }
            val name = curve::class.simpleName
            println("name: $name; time: ${time.inWholeMilliseconds}ms; ${position.toPrecision(1)}")
        }
    }
}