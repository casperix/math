package ru.casperix.collection

import ru.casperix.math.collection.DoublyLinkedList
import kotlin.test.Test
import kotlin.test.assertContentEquals

class DoublyLinkedListTest {

    fun setupList(): DoublyLinkedList<Int> {
        val items = DoublyLinkedList<Int>()
        items.pushFront(1)
        items.pushFront(2)
        items.pushFront(3)
        items.pushFront(4)
        items.pushBack(0)
        return items
    }

    @Test
    fun pushTest() {
        val items = setupList()

        assertContentEquals(listOf(4, 3, 2, 1, 0), items.values().toList())
    }

    @Test
    fun removeValueTest() {
        val items = setupList()

        items.remove(3)
        assertContentEquals(listOf(4, 2, 1, 0), items.values().toList())

        items.remove(4)
        assertContentEquals(listOf(2, 1, 0), items.values().toList())

        items.remove(0)
        assertContentEquals(listOf(2, 1), items.values().toList())

        items.remove(1)
        assertContentEquals(listOf(2), items.values().toList())

        items.remove(2)
        assertContentEquals(listOf(), items.values().toList())
    }

    @Test
    fun removeNodeTest() {
        val items = DoublyLinkedList<Int>()
       val node1= items.pushFront(1)
       val node2= items.pushFront(2)
       val node3= items.pushFront(3)
       val node4= items.pushFront(4)

        assertContentEquals(listOf(4, 3, 2, 1), items.values().toList())

        items.remove(node3)
        assertContentEquals(listOf(4, 2, 1), items.values().toList())

        items.remove(node3)
        assertContentEquals(listOf(4, 2, 1), items.values().toList())

        items.remove(node1)
        assertContentEquals(listOf(4, 2), items.values().toList())

        items.remove(node4)
        assertContentEquals(listOf(2), items.values().toList())

        items.remove(node2)
        assertContentEquals(listOf(), items.values().toList())
    }
}