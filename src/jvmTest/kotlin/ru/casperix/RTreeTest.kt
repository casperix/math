package ru.casperix

import ru.casperix.math.mesh.component.RTreeEdge
import ru.casperix.math.mesh.float32.MeshEdge
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import kotlin.test.Test
import kotlin.test.assertEquals


class RTreeTest {
    @Test
    fun test() {
        val tree = RTreeEdge(listOf(MeshEdge(Vector2f(1f, 0f), Vector2f(2f, 0f))))

        assertEquals(1, tree.search(Vector2f(1f, 0f), 0f).size)
        assertEquals(0, tree.search(Vector2f(0f, 0f), 0f).size)

        assertEquals(0, tree.search(LineSegment2f(Vector2f(0.9f, 0f), Vector2f(0f, 0f))).size)
        assertEquals(1, tree.search(LineSegment2f(Vector2f(1f, 0f), Vector2f(0f, 0f))).size)
        assertEquals(1, tree.search(LineSegment2f(Vector2f(1.1f, 0f), Vector2f(2f, 0f))).size)
        assertEquals(1, tree.search(LineSegment2f(Vector2f(1f, 0f), Vector2f(2f, 0f))).size)
        assertEquals(1, tree.search(LineSegment2f(Vector2f(1f, 0f), Vector2f(1.1f, 0f))).size)


    }
}