package ru.casperix.experimental

import ru.casperix.misc.DirectIntBuffer
import ru.casperix.misc.time.executeAndMeasure
import kotlin.test.Test

class JvmMapTest {


@Test
	fun performanceIntArray2D() {
		val size = 1024

		val (time, bitmap) = executeAndMeasure {
			val array = DirectIntBuffer(size * size)
			(0 until size).forEach { x ->
				(0 until size).forEach { y ->

//						fast
					array.put(x, x)

					//	slow
//					array.put(x + (y shl 10), x)

					//	slow
//					array[x + (y * 8192)] = x

					//	slow
//					array[x + (y * 8192)] = x + (y * 8192)

				}
			}
			array
		}

		println(bitmap.hashCode())
		println("time: $time ms; size: $size x $size; ")
	}

}